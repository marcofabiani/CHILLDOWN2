/*-----------------------------------------------------------------------------------------
 LIBRARY: CHILLDOWN_2
 FILE: Tube_Chilldown
 Authors: M.F., G.G., B.L., S.D. - Sapienza University of Rome
 CREATION DATE: 03/10/2022
-----------------------------------------------------------------------------------------*/
-- Libraries
USE MATH 
USE THERMO_TABLE_INTERP
USE PORTS_LIB
USE FLUID_PROPERTIES
USE THERMAL
USE FLUID_FLOW_1D


ABSTRACT COMPONENT ABS_Tube_Chilldown
(
    SET_OF(Chemicals_full) burnerGasesOption = noBurnGases "Select Chemicals, or a reduced SET_OF Chemicals, only for components downstream a burner",
    ENUM AbsorDesorOption AbsorOption = noActive    "Absorption/desorption option",
    INTEGER nodes = 5					"Number of control volumes",
    INTEGER n_bends = 1					UNITS no_units		"Number of bends",
    ENUM Scheme scheme = centred		"Numerical scheme used",
    ENUM UFM ufm = Damp_def			"Option to select the unsteady friction model",
    BOOLEAN wallExpOption = TRUE		"If TRUE, the wall expansibility is taken into account",
    BOOLEAN CAV_DAMP = FALSE			"If TRUE, better capture of liquid fronts. To be only used if necessary"
)
"Cylindrical area-varying non-uniform mesh high resolved 1D capacitive pipe with 1D heat transfer with the wall"

    PORTS
        IN fluid (burnerGasesOption = burnerGasesOption)  f1  CARDINALITY 1  "Inlet/Outlet fluid port"
        IN fluid (burnerGasesOption = burnerGasesOption)  f2  CARDINALITY 1  "Inlet/Outlet fluid port"
        OUT pipe_measure (n = 3*nodes)  meas_out  "Pipe outlet signals"
--        IN  mass_add(n = nodes) mf_in  "Additional mass flow port"     -- JP additional parameter for mass addition
        OUT GasConc (burnerGasesOption = burnerGasesOption) bu_eq[nodes]  "Mass fractions of burned gases"

    DATA
        REAL num = 1					"Number of parallel tubes"
	 	    ENUM Order order = first	"Precision order in the reconstruction"

        ENUM INIT_OPTION init_option = INIT_PT  "Option to specify the initial thermodynamic state"
        REAL P_o = 100000			UNITS u_Pa		"Initial pressure"
        REAL T_o = 293.15			UNITS u_K		"Initial temperature"
        REAL x_o = 0					UNITS no_units	"Initial quality"
        REAL rho_o = 1				UNITS u_kg_m3	"Ratio of initial liquid mass inside the pipe divided by the total pipe volume"
        REAL x_nco = 0.				UNITS no_units	"Initial non-diluted, non-condensable gas mass fraction - only for INIT_PT or INIT_RT options"
        REAL m_o =  0.				UNITS u_kg_s	"Initial mass flow"

        REAL rug = 0.05e-3			UNITS u_m		"Roughness"
        REAL k_f = 1					UNITS no_units	"Multiplier of the steady friction factor"
        REAL k_d = 1					UNITS no_units	"Multiplier of the unsteady friction - or numerical dissipation"
        REAL fld_add= 0				UNITS no_units	"Pressure drop coefficient for additional losses different than friction"
        REAL fld_nod[nodes]= 0	UNITS no_units	"Nodal pressure drop coefficient for not distributed losses different than friction"

        REAL alpha_bend[n_bends] = 0	UNITS u_deg	"Bend angles"
        REAL R_bend[n_bends] = 1			UNITS u_m	"Bend curvature radii"
-- CHILLDOWN OPTIONS START
        ENUM FR_OPTION fr_option =  FR_tube_1ph		"Wall-internal fluid friction correlation option"
		    ENUM HT_OPT_CHILL	  ht_option		=  HT_auto         "Wall-internal fluid heat transfer option"
        ENUM HT_OPT_CHILL_FB ht_option_FB =  HT_const_FB		"Wall-internal fluid heat transfer option - Film boiling model"
        ENUM HT_OPT_CHILL_TB ht_option_TB =  HT_const_TB		"Wall-internal fluid heat transfer option - Transition boiling model"
        ENUM HT_OPT_CHILL_NB ht_option_NB =  HT_const_NB		"Wall-internal fluid heat transfer option - Nucleate boiling model"
		    ENUM HT_OPT_CHILL_TW ht_option_TW =  HT_const_TW		"Wall-internal fluid heat transfer option - Rewet temperature model"
		    ENUM HT_OPT_CHILL_Ton ht_option_Ton = HT_const_Ton		"Wall-internal fluid heat transfer option - Nucleate boiling onset model"
        ENUM HT_OPT_CHILL_Qcr ht_option_qcr  = HT_darr2015_qcr     "Heat transfer option - Critical heat flux"  
		    REAL htc_dat			= 100					UNITS u_W_m2K	"Heat transfer coefficient"
        REAL hc_FB_dat 		= 100					UNITS u_W_m2K	"Heat transfer coefficient - only if ht_option_FB = Ht_const_FB"
        REAL hc_TB_dat 		= 1500				UNITS u_W_m2K	"Heat transfer coefficient - only if ht_option_TB = Ht_const_TB"
        REAL hc_NB_dat 		= 1000				UNITS u_W_m2K	"Heat transfer coefficient - only if ht_option_NB = Ht_const_NB"
		    REAL Twet_dat 	= 120						UNITS u_K		"Wall rewet temperature (K) - only if ht_option_TW = HT_const_TW"
		    REAL Ton_dat    = 90						UNITS u_K      "Nucleate boiling onset temperature (K) - only if ht_option_Ton = HT_const_Ton"
        REAL qcr_dat    = 1e3                UNITS u_W_m2   "Imposed critical heat flux if ht_option_qcr == ht_const"
				REAL h_f	      = 1							UNITS no_units "Multiplier of the heat transfer coefficient"		
				ENUM Material mat = None		UNITS no_units			"Wall material"
				BOOLEAN TemperatureDependance = TRUE	"Option to consider or not the temperature depence on the material properties"
		    REAL rhos	= 8000		UNITS u_kg_m3  "wall material density"
		    REAL cps	= 500 		UNITS u_J_kgK  "wall material specific heat"
		    REAL ks = 16.2			UNITS u_W_mK	"wall material thermal conductivity"
				
-- CHILLDOWN OPTIONS END
        -- numerical parameters of the component shown in the GUI
        BOOLEAN Isent_Correl = FALSE				"Only for Roe schemes, TRUE supersonic flow allowed"
        ENUM Roe_Fix entropy_fix = no_fix			"Expert mode: Entropy fix used"
        REAL entropy_fix_multiplier = 4			UNITS no_units	"Expert mode: Multiplier in the entropy fix"
        ENUM Int_Rules integration_rule = midpoint  "Expert mode: Numerical integration rule for pressure derivatives"
        BOOLEAN dp_correction = FALSE				"Expert mode: Correction on pressure derivative linearizations"
        ENUM Limiter limiter = VanAlbada			"Expert mode: Limiter used"
        ENUM Precond preconditioner = unprecond	"Expert mode: Preconditioner used"
        ENUM Reconstructed_Variables reconstructed_variables = primitive "Expert mode: Reconstructed variables"
        BOOLEAN central_reconstruction = TRUE	"Expert mode: Reconstruction on central part of numerical flux"
        REAL source_upwind_smoothing = 0			UNITS no_units	"Expert mode: Source term upwinding smoothing parameter"  

        -- Solubility data
---------------------Pinna Edit 18 Jul 2013--START------------------------           
        REAL xd_nco = 0				UNITS no_units	"Initial gas mass fraction diluted in the liquid phase"
		    REAL Po_nc = 1e5			UNITS u_Pa		"Pressure at which the gas was dissolved - It should be the same for a whole circuit"
        BOOLEAN UserDefSolubData = TRUE    "TRUE, user input data"
        REAL A_coef_sol = -521	" A_coef. for binary solubility calculation -only if UserDefSolubData = TRUE"
        REAL B_coef_sol = 2.3874	" B_coef. for binary solubility calculation -only if UserDefSolubData = TRUE"
        REAL TI = 0.03				"Turbulence intensity, free stream speed" -- user should modify this        
        REAL Cd = 2.0				UNITS "m s/kg"	"Empirical coefficient for desorption of non-condensable gases" 
        REAL Ca = 0.1				UNITS "m s/kg"	"Empirical coefficient for absorption of non-condensable gases"
        REAL tau_d = 0.3			UNITS u_s		"Time constant for desorption of non-condensable gases" 
        REAL tau_a = 2				UNITS u_s		"Time constant  for absorption of non-condensable gases"
        REAL Diff_Turb_Factor=1	UNITS no_units	"Factor weighting diffusion of diluted gases"
        REAL K_u =  0.25		"AUSM velocity diffusion term coeff. for interface pressure"
        REAL K_p =  0.75		"AUSM pressure diffusion term coeff. for interface Mach"


    DECLS
        CLOSE bu_eq
        ENUM Chemicals_full chem
        REAL Dh[nodes]				UNITS u_m		"Cell diameter array"
        REAL A[nodes]				UNITS u_m2		"Cell cross section array"
        REAL V[nodes]				UNITS u_m3		"Cell volume array"
        REAL dL[nodes]				UNITS u_m		"Cell mesh size array"      
        DISCR REAL RdL[nodes+1,3]	UNITS u_m	"Cell mesh coordinate vector wrt the Mechanical Reference Frame centre O --i.e. wrt launcher interface at point O--" 
        REAL A_in, A_out			UNITS u_m2		"Inlet and outlet cross-sections"
        REAL L_r = 1					UNITS u_m		"Straight pipe length"
--			HIDDEN REAL Dl_pnts		"cell size for a even distribution of the nodes"
        EXPL REAL g[nodes]			UNITS u_m_s2	"Scalar universal gravitational acceleration"
        REAL zeta_bends				UNITS no_units	"Loss coefficient due to the bends"
        REAL kappa_wall[nodes]	UNITS u_1_Pa	"Compressibility due to wall expansibility"

        REAL  cp[nodes]				UNITS u_J_kgK	"Specific heat at constant pressure"
        REAL  cpf[nodes]			UNITS u_J_kgK	"Specific heat at constant pressure of saturated liquid"
        REAL  cpg[nodes]			UNITS u_J_kgK	"Specific heat at constant pressure of saturated vapor"
        REAL  cond[nodes]			UNITS u_W_mK	"Conductivity of fluid volumes" 
        REAL  condf[nodes]			UNITS u_W_mK	"Conductivity of saturated liquid" 
        REAL  condg[nodes]			UNITS u_W_mK	"Conductivity of saturated vapor" 
        REAL  visc[nodes]			UNITS u_Pas		"Viscosity of the fluid volumes"
        REAL  viscf[nodes]			UNITS u_Pas		"Viscosity of saturated liquid"
        REAL  viscg[nodes]			UNITS u_Pas		"Viscosity of saturated vapor"

        REAL  vsound[nodes]		UNITS u_m_s		"Sound speed of fluid volumes"
        HIDDEN REAL  drho_dp[nodes]=1	UNITS "s^2/m^2"		"drho/dp at constant h"
        HIDDEN REAL  drho_dh[nodes]=1	UNITS "kg*s^2/m^5"	"drho/dh at constant p"
        HIDDEN REAL  dP[nodes]			UNITS u_Pa_s	"Derivative of the nodal pressure"

        REAL  h[nodes]				UNITS u_J_kg	"Mixture -gas and liquid- total enthalpies"
        REAL  hf[nodes]				UNITS u_J_kg	"Enthalpy of saturated liquid"
        REAL  hg[nodes]				UNITS u_J_kg	"Enthalpy of saturated vapor"
        REAL  Mach[nodes]			UNITS no_units	"Mach numbers"
        REAL  P[nodes]				UNITS u_Pa		"Static pressure of volumes"
        REAL  P_nc[nodes]			UNITS u_Pa		"Non-condensable partial pressure in the volumes"
        REAL  rho[nodes]			UNITS u_kg_m3	"Mixture -gas and liquid- densities"
        REAL  rhof[nodes]			UNITS u_kg_m3	"Density of saturated liquid"
        REAL  rhog[nodes]			UNITS u_kg_m3	"Density of saturated vapor"
        REAL  sigma[nodes]			UNITS u_N_m		"Surface tension"
        REAL  T[nodes]				UNITS u_K		"Static temperature of the fluid volumes"
        REAL  Tsat[nodes]			UNITS u_K		"Saturation temperature in volumes"
        REAL  u[nodes]				UNITS u_J_kg	"Mixture -gas and liquid- total energy"
        HIDDEN REAL  ust[nodes]	UNITS u_J_kg	"Mixture -gas and liquid- static energy"
        REAL  vel[nodes]			UNITS u_m_s		"Average velocity of the fluid volumes"
        REAL  acc[nodes]			UNITS u_m_s2	"Average acceleration of the fluid volumes"
        REAL  dvel_dx[nodes]		UNITS u_1_s		"Convective acceleration"
        REAL  T_wall[nodes]		UNITS u_K		"Temperature of the wall nodes in contact with the fluid"

        ENUM  Phase phase[nodes]	"Phase of the fluid volumes"
        REAL  alpha[nodes]			UNITS no_units	"Void fraction in fluid volumes"
        REAL  x[nodes]				UNITS no_units	"Quality -gas mass fraction- including vapour and non-condensable gases"
        REAL  x_nc[nodes]			UNITS no_units	"Non-condensable mass fractions relative to the total fluid mass"        
        REAL  xd_nc[nodes]			UNITS no_units	"Non-condensable mass fraction diluted in the liquid phase" 
--        REAL  m_abs[nodes		UNITS u_kg_s	"Absorption/desorption gas mass flow in the liquid phase"
        REAL  md_nc_jun[nodes+1]	UNITS u_kg_s	"Diluted gas mass flow at unions between volumes"

        EXPL REAL  Re[nodes]		UNITS no_units	"Reynolds numbers"
        EXPL REAL  fr[nodes]		UNITS no_units	"Wall friction factors"
        EXPL REAL  zeta[nodes]	UNITS no_units	"Equivalent distributed pressure drop coefficient of the fluid volumes"
        REAL  k_fru[nodes]			UNITS no_units	"Coefficient for unsteady friction"
        REAL  zeta_uf[nodes]
        EXPL REAL  hc[nodes]		UNITS u_W_m2K	"Wall heat transfer coefficients"
        REAL  q[nodes]				UNITS u_W		"Heat flow at internal walls"
        EXPL REAL  qn[nodes]		UNITS u_Pa		"Pressure contribution due to artificial viscosity"
        REAL  m_cel[nodes]			UNITS u_kg_s	"Mass flow in the volumes"
        REAL  m_dot[nodes]      UNITS u_kg_s2 
				REAL  m_jun[nodes+1]		UNITS u_kg_s	"Mass flow in the junctions"
        HIDDEN REAL k_dd[nodes]	UNITS no_units	"Auxiliary variable for unsteady friction model"
		  
		    REAL mass						UNITS u_kg		"Total fluid mass of the pipe (gas + liquid)"
		    REAL mass_g					UNITS u_kg		"Total gas mass of the pipe"
		    REAL mass_l					UNITS u_kg		"Total liquid mass of the pipe"
			
--------- Pinna's Edit 29 Aug 2013 --- START -------------------------------------------
--			REAL num_f_diff_liq[nodes+1]
	--		REAL num_f_diff_gas[nodes+1]   
--------- Pinna's Edit 29 Aug 2013 ---- END --------------------------------------------

        HIDDEN REAL  num_f_mass[nodes+1] "Numerical mass flux vector at each cell interface"
        HIDDEN REAL  num_f_nc[nodes+1]   "Numerical non-condensable mass flux vector at each cell interface"
        HIDDEN REAL  num_f_mom[nodes+1]  "Numerical momentum flux vector at each cell interface"
        HIDDEN REAL  num_f_ene[nodes+1]  "Numerical energy flux vector at each cell interface"

        INTEGER  ier[nodes]=0          "Error index of thermodynamic function calls"
        HIDDEN INTEGER  ipx[nodes]     "last index for x interpolation"
        HIDDEN INTEGER  ipy[nodes]     "last index for y interpolation"
        HIDDEN INTEGER  ier1           "Error index of critic flow function call"

        -- ghost cell related properties

        HIDDEN REAL x_nc_gh_0   = 0    "Non-condensable mass fraction in ghost cell 0"
        HIDDEN REAL x_nc_gh_np1 = 0    "Non-condensable mass fraction in ghost cell n+1"
        HIDDEN REAL rho_gh_0    = 1    "Density in ghost cell 0"
        HIDDEN REAL rho_gh_np1  = 1    "Density in ghost cell n+1"
        HIDDEN REAL vel_gh_0    = 1    "Velocity in ghost cell 0"
        HIDDEN REAL vel_gh_np1  = 1    "Velocity in ghost cell n+1"
        HIDDEN REAL P_gh_0      = 1e5  "Pressure in ghost cell 0"
        HIDDEN REAL P_gh_np1    = 1e5  "Pressure in ghost cell n+1"
        HIDDEN REAL A_gh_0      = 1.   "Cross section in ghost cell 0"
        HIDDEN REAL A_gh_np1    = 1.   "Cross section in ghost cell n+1"
        
        HIDDEN REAL T_gh_0   = 300   --  JP
        HIDDEN REAL T_gh_np1 = 300   --  JP
        HIDDEN REAL P_nc_gh_0   = 1e5   --  JP
        HIDDEN REAL P_nc_gh_np1 = 1e5   --  JP
        HIDDEN REAL u_gh_0  = 1.e5     "Static energy in ghost cell n+1" 
        HIDDEN REAL u_gh_np1  = 1.e5   "Static energy in ghost cell n+1"
        HIDDEN REAL x_gh_0    = 1.     "Global quality in ghost cell 0"
        HIDDEN REAL x_gh_np1  = 1.     "Global quality in ghost cell n+1"
        HIDDEN REAL alpha_gh_0    = 1. "Void fraction in ghost cell 0"
        HIDDEN REAL alpha_gh_np1  = 1. "Void fraction in ghost cell n+1"
        HIDDEN REAL vs_gh_0    = 1.    "Sound speed in ghost cell 0"
        HIDDEN REAL vs_gh_np1  = 1.    "Sound speed in ghost cell n+1"
        -- source term related variables

--        HIDDEN REAL s_ncd_jun[nodes+1]   = 0. "Junction-wise diluted non-condensable mass fraction source term vector"
        HIDDEN REAL s_mass_jun[nodes+1] = 0. "Junction-wise mass source term vector"
        HIDDEN REAL s_nc_jun[nodes+1]   = 0. "Junction-wise non-condensable mass fraction source term vector"
        HIDDEN REAL s_mom_jun[nodes+1]  = 0. "Junction-wise momentum source term vector"
        HIDDEN REAL s_ene_jun[nodes+1]  = 0. "Junction-wise energy source term vector"

--        HIDDEN REAL s_ncd_cel[nodes]   = 0.   "Cell-wise non-condensable mass fraction source term vector"
        HIDDEN REAL s_mass_cel[nodes] = 0.   "Cell-wise mass source term vector"
        HIDDEN REAL s_nc_cel[nodes]   = 0.   "Cell-wise non-condensable mass fraction source term vector"
        HIDDEN REAL s_mom_cel[nodes]  = 0.   "Cell-wise momentum source term vector"
        HIDDEN REAL s_ene_cel[nodes]  = 0.   "Cell-wise energy source term vector"

--        HIDDEN REAL s_ncd_p[nodes+1]   = 0.   "Junction-wise positive splitted non-condensable mass source term vector"
        HIDDEN REAL s_mass_p[nodes+1] = 0.   "Junction-wise positive splitted mass source term vector"
        HIDDEN REAL s_nc_p[nodes+1]   = 0.   "Junction-wise positive splitted non-condensable mass source term vector"
        HIDDEN REAL s_mom_p[nodes+1]  = 0.   "Junction-wise positive splitted momentum source term vector"
        HIDDEN REAL s_ene_p[nodes+1]  = 0.   "Junction-wise positive splitted energy source term vector"

--        HIDDEN REAL s_ncd_m[nodes+1]   = 0.   "Junction-wise negative splitted non-condensable mass source term vector"
        HIDDEN REAL s_mass_m[nodes+1] = 0.   "Junction-wise negative splitted mass source term vector"
        HIDDEN REAL s_nc_m[nodes+1]   = 0.   "Junction-wise negative splitted non-condensable mass source term vector"
        HIDDEN REAL s_mom_m[nodes+1]  = 0.   "Junction-wise negative splitted momentum source term vector"
        HIDDEN REAL s_ene_m[nodes+1]  = 0.   "Junction-wise negative splitted energy source term vector"

        REAL Ra[nodes]           "Rate of adsorption for the non condensable gas mass conservation equations"
        REAL Rd[nodes]           "Rate of desorption for the non condensable gas mass conservation equations"
        REAL Diff[nodes]         "Diffusion coefficient into liquid [m^2/s]"

        HIDDEN REAL source_term_mass[nodes]  "Source term vector of the mass conservation equation"
        HIDDEN REAL source_term_nc[nodes]    "Source term vector of the non-condensable mass fraction conservation equation"
        HIDDEN REAL source_term_ncd[nodes]   "Source term vector of the non-condensable mass fraction conservation equation of the diluted gas"
        HIDDEN REAL source_term_mom[nodes]   "Source term vector of the momentum conservation equation"
        HIDDEN REAL source_term_ene[nodes]   "Source term vector of the energy conservation equation"

		    ----------Leonardi Edit started   24/10/2013 ----START--------------------
	  		--AUSM
	  		REAL P_AUSM[nodes+1]    "Interface pressure calculated by AUSM"
	  		REAL s_mom_AUSM[nodes]
	  		REAL A_int[nodes+1]     "Interface area"

	  		REAL H_gh_0 		"Total Enthalpy in first ghost cell"

----------Leonardi Edit started   30/10/2014 ----END--------------------

        -- numerical parameters of the component

        --ENUM Order order        "Precision order in the reconstruction"
        ENUM Roe_Fix fix        "Entropy fix used"
        DISCR REAL mult         "Multiplier in the entropy fix"
        ENUM Int_Rules rule     "Numerical integration rule for pressure derivatives"
        BOOLEAN dp_cor          "Correction of pressure derivative linearizations"
        ENUM Limiter lim        "Limiter used"
        ENUM Precond precond    "Preconditioner used"
        ENUM Reconstructed_Variables var_rec "Primitive or conservative reconstruction"
        BOOLEAN central         "Reconstruction on central part of numerical flux"
        DISCR REAL eps_lin      "Source term upwinding smoothing parameter"

     -- auxiliary variables not depending on the node number
        REAL T1			UNITS u_K
        REAL Tn			UNITS u_K
        REAL P1			UNITS u_Pa
        REAL Pn			UNITS u_Pa
        REAL alpha1		UNITS no_units
        REAL alphan		UNITS no_units
        REAL vsound1		UNITS u_m_s
        REAL vsoundn		UNITS u_m_s
        REAL vel1			UNITS u_m_s
        REAL veln			UNITS u_m_s
        REAL m1			UNITS u_kg_s
        REAL mn			UNITS u_kg_s
        REAL rho1			UNITS u_kg_m3
        REAL rhon			UNITS u_kg_m3
        REAL arhovel1	UNITS u_Pa		"Joukowsky formula"
        REAL arhoveln	UNITS u_Pa		"Joukowsky formula"
        REAL inertance_node	UNITS u_1_m
        REAL dP_loss		UNITS u_Pa		"Static pressure drop in the pipe"
        CLOSE CAV_DAMP = FALSE

        HIDDEN DISCR REAL x_chem_1[burnerGasesOption]			"Chemicals mass fractions in the mixture for init purposes"
        HIDDEN REAL x_chem[burnerGasesOption,nodes]			"Chemicals mass fractions in the mixture"
        HIDDEN REAL m_nx_jun[burnerGasesOption,nodes+1]	UNITS u_kg_s	"Chemicals mass flow at unions between volumes"
        BOOLEAN Init_Flag
--		  REAL P_num[nodes]
        DISCR REAL dL_rfi[nodes]
		  
		    REAL x_s[nodes]		UNITS no_units	"Solid mass fraction in the liquid phase"
	   	  REAL lp[nodes]		UNITS u_m		"Pipe node distances from inlet"
--        DISCR REAL rho_old[nodes], ust_old[nodes], x_nc_old[nodes]
-- new variables for chilldown
		  	REAL check[nodes] UNITS no_units "Boiling regime identification flag"
		  	REAL Ton[nodes]   UNITS u_K      "Nucleate boiling onset temperature"
		  	REAL Twet[nodes]  UNITS u_K      "Rewetting temperature"
		  	REAL Tcrit[nodes]	UNITS u_K		"Critical temperature (K)"
		   	REAL Qcr[nodes]   UNITS u_W_m2   "Critical heat flux"
		  	REAL Tsens[nodes] UNITS u_K		"Sensor temperature"
  			--REAL flag[nodes]
				INTEGER pointcp,pointk  "pointers output of FunVarSolidProp"
				REAL cp_s[nodes], rho_s, k_s[nodes]
    INIT
        Init_Flag = TRUE

        IF (ChemCode[f1.fluid] < 0) THEN
           chem = setofElem(Chemicals_full,ChemCode[f1.fluid_nc])
        END IF
        ASSERT(AbsorOption == noActive OR f1.fluid_nc != NoFluid) FATAL "With AbsorOption = Active use a non-condensable fluid"
--WRITE("\nINIT pipe: fluid=%s fluid_nc=%s chem=%s",gvalEnumByType(FluidKeys,f1.fluid),gvalEnumByType(FluidKeys,f1.fluid_nc),gvalEnumByType(Chemicals,chem))

          -- numerical configuration
          fix     = entropy_fix
          mult    = entropy_fix_multiplier
          rule    = integration_rule
          dp_cor  = dp_correction
          lim     = limiter
          precond = preconditioner
          var_rec = reconstructed_variables
          central = central_reconstruction
          eps_lin = source_upwind_smoothing

        FOR (i IN 1,nodes)
           -- Chemical initialisation
           FOR(j IN burnerGasesOption)
              x_chem[j,i] = 0 
              x_chem_1[j] = 0 
           END FOR
           IF (ChemCode[f1.fluid] < 0) THEN
              x_chem[chem,i] = 1
              x_chem_1[chem] = 1
           END IF
           xd_nc[i] = xd_nco

           -- State vars initialisation
           Init_Vol(burnerGasesOption, x_chem_1, f1.fluid, f1.fluid_nc, init_option, x_nco, P_o, T_o, x_o, rho_o, 
                   P[i], P_nc[i], x_nc[i], u[i], T[i], rho[i], vsound[i], x[i], alpha[i], visc[i], cond[i], cp[i], ipx[i],ipy[i],ier[i])
           ASSERT(ier[i] < 100) FATAL "Simulation stoped"
--           P_num[i]   = P[i]

           m_cel[i] = m_o/num
--           mf_in.m_add[i] = 0 --delta_ma[1]/nodes   -- JP mass addition: temporary definition for only one equally distributed mass flow
        END FOR

        FOR (i IN 1,nodes+1)
          m_jun[i]     = m_o/num
        END FOR 
--        IF(AbsorOption == Active) THEN
--           Po_nc = rho[1]*xd_nco/10**(A_coef_sol/T+B_coef_sol-8) 
--        END IF
		  

    DISCRETE

        -- Thermodynamic function calls check
        ASSERT (f1.n_fluid > 0) FATAL "ABS_Tube: working fluid not defined"
        ASSERT (ChemCode[f1.fluid] >= 0 OR ChemCode[f1.fluid_nc] > 0)  \
           FATAL "ABS_Tube: A non-condensable fluid compatible with Chemicals must be defined in case of combusted gases"

        EXPAND (i IN 1, nodes) ASSERT (ier[i]!=1 AND ier[i]!=7 AND ier[i]!=8)  ERROR "ABS_Tube: severe error in thermodynamic functions. Probably, solid phase is appearing"
        EXPAND (i IN 1, nodes) ASSERT (ier[i]!=20)                             ERROR "ABS_Tube: lack of convergence in H.E. Model"
--        EXPAND (i IN 1, nodes) ASSERT(T[i] > 1)    FATAL "ABS_Tube: Temperature < 1 K: Check the input data or boundary conditions ..."
        EXPAND (i IN 1, nodes) ASSERT (ier[i]!=30) ERROR "ABS_Tube: Melting solid conditions not allowed with a mixture of NCG !!"

        --  Bubble collapse: 

        EXPAND_BLOCK(CAV_DAMP) 
        EXPAND_BLOCK(i IN 1, nodes)  
          WHEN(alpha[i] <= 1e-5) THEN
             IF(vel[i] > 0.1 AND i != nodes  AND alpha[i+1]>1e-1) THEN
               m_jun[i+1] = m_jun[i]
             ELSEIF(vel[i] < -0.1 AND i != 1 AND alpha[i-1]>1e-1) THEN
               m_jun[i] = m_jun[i+1]
             END IF
          END WHEN
        END EXPAND_BLOCK
        END EXPAND_BLOCK

        -- the following is to take into account the gravity initialising densities and pressures

        WHEN(Init_Flag) THEN
          L_r = SUM(i IN 1,nodes; dL[i]) -- sqrt(max(1e-6, (f1.z_jun-f2.z_jun)**2 + (f1.x_jun-f2.x_jun)**2 + (f1.y_jun-f2.y_jun)**2))  -- 
          FOR (i IN 1,nodes+1) 
			     FOR (j IN 1,3) 
			      RdL[i,j] = f1.Rj[j]+(f2.Rj[j]-f1.Rj[j])*(i-1)/nodes --approximation with constant repartition
			     END FOR
          END FOR

          FOR (i IN 1,nodes) 
			     dL_rfi[i]=max(0.001, sqrt(SUM(j IN 1,3;(RdL[i,j]-RdL[i+1,j])**2)))-- real length between ports .fi
           IF(FlagFrame) THEN
				    g[i] = p2_p1Index(OCom, nodes+1,i,i+1,RdL,rho[i])/rho[i]/dL_rfi[i]  
           ELSE
				    g[i] = (GRAV*(f1.z_jun-f2.z_jun) + GRAVx*(f1.x_jun-f2.x_jun) + GRAVy*(f1.y_jun-f2.y_jun))/L_r
           END IF
          END FOR

			 ASSERT (L_r > sqrt((f1.z_jun - f2.z_jun)**2 + (f1.x_jun - f2.x_jun)**2 + (f1.y_jun - f2.y_jun)**2)) \
			 			ERROR "The Pipe length L must be greater than the distance between the pipe ends (x_y_z_jun coordinates)"

          IF(x[1] == 0 AND abs(g[1]) > 0.001 AND init_option == INIT_PT) THEN
             P[1]   = P[1] + rho[1] * g[1] * dL[1] /2
             rho[1] = FL_prop_vs_pT(burnerGasesOption,f1.x_eq, f1.fluid, P[1], T[1], fprop_density,ier[1])
             u[1]   = FL_prop_vs_pT(burnerGasesOption,f1.x_eq, f1.fluid, P[1], T[1], fprop_energy, ier[1]) + 0.5 * (m_o/num/rho[1]/A[1])**2

             FOR (i IN 2,nodes)
                P[i]   = P[i-1] + rho[i] * g[i] * dL[i]
                rho[i] = FL_prop_vs_pT(burnerGasesOption,f1.x_eq, f1.fluid, P[i], T[i], fprop_density,ier[i])
                u[i]   = FL_prop_vs_pT(burnerGasesOption,f1.x_eq, f1.fluid, P[i], T[i], fprop_energy, ier[i]) + 0.5 * (m_o/num/rho[i]/A[i])**2
					 
             END FOR
          END IF

          Init_Flag = FALSE
        END WHEN
		  WHEN(ht_option_FB == HT_darr2015) THEN
			ASSERT (lp[nodes]/Dh[nodes]<100) ERROR "L/D>(L/D)max for Darr 2015"
			END WHEN

--		  ASSERT(FlagFrame==TRUE) FATAL "ESPSS models need a Frame or a BoundGrav component."

    CONTINUOUS
	 -- computation of wall thermal properties
	 	rho_s = FunConstSolidProp(mat, Density, rhos)
			EXPAND_BLOCK(i IN 1, nodes)
				cp_s[i] =	ZONE (TemperatureDependance) FunVarSolidProp(mat, SpecificHeat, T_wall[i], cps, pointcp)
									OTHERS                       FunVarSolidProp(mat, SpecificHeat, T_o, cps, pointcp)
                              
				k_s[i] =	ZONE (TemperatureDependance) FunVarSolidProp(mat, Conductivity, T_wall[i], ks, pointk)
									OTHERS                       FunVarSolidProp(mat, Conductivity, T_o, ks, pointk)
         
			END EXPAND_BLOCK  
		-- Pipe node distances from inlet
		SEQUENTIAL
			lp[1] = dL[1]/2
			FOR (i  IN 2,nodes)
				lp[i] = lp[i-1] + dL[i-1]/2 + dL[i]/2
			END FOR
		END SEQUENTIAL

		-- Internal Pipe junctions areas
	 	  EXPAND(i IN 2,nodes)  A_int[i] = 0.5 *(A[i-1] + A[i])
        A_int[1] = A_in
        A_int[nodes+1] = A_out

        -- We need to use a duplicate (as port vars) of the composition variables to be used as arrays in properties function
        EXPAND_BLOCK (j IN 1, nodes)
        EXPAND_BLOCK (i IN burnerGasesOption) 
           bu_eq[j].x[i] = x_chem[i,j]
        END EXPAND_BLOCK
        END EXPAND_BLOCK

        -- Boundary conditions
        Ports_Handling_Cap(nodes,f1.fluid,f1.fluid_nc,scheme,Isent_Correl,A_in,f1.Q,f1.m,f1.m_nc,f1.mh,\
                           A_out,-f2.Q,-f2.m,-f2.m_nc,-f2.mh,A[1],A[nodes],P[1],P[nodes],x_nc[1],x_nc[nodes],\
                           rho[1],rho[nodes],h[1],h[nodes],vel[1],vel[nodes],\
                           P_gh_0,P_gh_np1,x_nc_gh_0,x_nc_gh_np1,rho_gh_0,rho_gh_np1,\
                           vel_gh_0,vel_gh_np1,A_gh_0,A_gh_np1,P_nc_gh_0,P_nc_gh_np1,T_gh_0,T_gh_np1,\
                           x_gh_0,x_gh_np1,alpha_gh_0,alpha_gh_np1,u_gh_0,u_gh_np1,vs_gh_0,vs_gh_np1)

        -- output variables       
        EXPAND(i IN burnerGasesOption) f1.x_eq[i] = IF(ChemCode[f1.fluid] >= 0) 0  ELSE  x_chem[i,1]
        EXPAND(i IN burnerGasesOption) f2.x_eq[i] = IF(ChemCode[f1.fluid] >= 0) 0  ELSE  x_chem[i,nodes]

        L_r = SUM(i IN 1,nodes; dL[i]) -- sqrt(max(1e-6, (f1.z_jun-f2.z_jun)**2 + (f1.x_jun-f2.x_jun)**2 + (f1.y_jun-f2.y_jun)**2)) -- 

        EXPAND_BLOCK(i IN 1,nodes)
		  g[i] =	IF(FlagFrame)	p2_p1Index(OCom, nodes+1, i, i+1, RdL, rho[i]) /rho[i]/dL_rfi[i] 
		 			ELSE          (GRAV*(f1.z_jun-f2.z_jun) + GRAVx*(f1.x_jun-f2.x_jun) + GRAVy*(f1.y_jun-f2.y_jun))/L_r
        END EXPAND_BLOCK

        f1.fluid = f2.fluid
        f1.fluid_nc = f2.fluid_nc
        f1.n_fluid = f2.n_fluid

        f1.P = P[1]     + 0.25*zeta[1]    *rho[1]    *vel[1]    *abs(vel[1])     + 0.25*zeta_uf[1]    *rho[1]     - 0.50*rho[1]*g[1]*dL[1]  + qn[1] 
        f2.P = P[nodes] - 0.25*zeta[nodes]*rho[nodes]*vel[nodes]*abs(vel[nodes]) - 0.25*zeta_uf[nodes]*rho[nodes] + 0.50*rho[nodes]*g[nodes]*dL[nodes] + qn[nodes]
        f1.T = T[1]    
        f2.T = T[nodes]

        f1.x_nc  = x_nc[1]
        f2.x_nc  = x_nc[nodes]
        f1.xd_nc = xd_nc[1]
        f2.xd_nc = xd_nc[nodes]

        f1.h = h[1]     -- - 0.50 * g[1] * dL[1]  
        f2.h = h[nodes] -- + 0.50 * g[nodes] * dL[nodes]

        f1.v  = vel[1] 
        f2.v  = vel[nodes] 
        f1.rho = rho[1]
        f2.rho = rho[nodes]
        f1.cp  = cp[1]
        f2.cp  = cp[nodes]
        f1.visc = visc[1]
        f2.visc = visc[nodes]
        f1.cond = cond[1]
        f2.cond = cond[nodes]

        f1.I = 0.5 * dL[1] / A[1] /num
        f2.I = 0.5 * dL[nodes] / A[nodes] / num
        f1.A = num * A_in
        f2.A = num * A_out

        f1.Gcrit = FL_Gcrit_fun(f1.fluid, P[1], rho[1], T[1], P_nc[1], vel[1], vsound[1], ier1)
        f2.Gcrit = FL_Gcrit_fun(f1.fluid, P[nodes], rho[nodes], T[nodes], P_nc[nodes], vel[nodes], vsound[nodes], ier1)
   
      -- Thermodynamic and transport properties
		
        EXPAND_BLOCK(i IN 1, nodes)   
          ust[i] = u[i] - 0.5*vel[i]**2
		-- IF (INSTANCE_NAME == "NTOp5") INSERT
		--	 PARALLEL INSTANCE_NAME
          FL_state_vs_ru(burnerGasesOption, bu_eq[i].x, f1.fluid, f1.fluid_nc, rho[i], ust[i], x_nc[i], P_nc[i], phase[i], 
                         rhof[i], rhog[i], P[i], T[i], Tsat[i], hf[i], hg[i], 
                         x[i], x_s[i], alpha[i], cp[i], cpf[i], cpg[i], drho_dp[i], drho_dh[i], vsound[i], 
                         visc[i], viscf[i], viscg[i], cond[i], condf[i], condg[i], sigma[i], ipx[i], ipy[i], ier[i])
          h[i]    = u[i] + P[i] / rho[i] 

          -- Auxiliary variables calculation (friction, Reynolds, film coef., etc)
          Mach[i] = abs(vel[i]) / vsound[i] 
          Re[i]   = rho[i]*abs(vel[i])*Dh[i]/visc[i]

          fr[i]   = hdc_fric_2phase(fr_option, Dh[i], rug, Re[i], m_cel[i], x[i], alpha[i], rhof[i], rhog[i], viscf[i], viscg[i], sigma[i], GRAV, Tsat[i])
          zeta[i] = fld_nod[i] + fld_add/nodes + zeta_bends/nodes + k_f*fr[i]*dL[i]/Dh[i]
			

			    Tcrit[i] = Tcr_comp(f1.fluid)
					m_dot[i] = (num_f_mom[i] - num_f_mom[i+1] + source_term_mom[i])/(dL[i])					
			    hc[i] = htc_fun_chilldown(ht_option,ht_option_FB, ht_option_TB, ht_option_NB, ht_option_TW, ht_option_Ton, ht_option_qcr, \
				                            htc_dat, hc_FB_dat, hc_TB_dat, hc_NB_dat, Twet_dat, Ton_dat, qcr_dat,h_f,\
					                          m_cel[i], m_dot[i], A[i], Dh[i], phase[i],\
                                    P[i], T[i], Tsat[i], Tcrit[i], h[i], hf[i], hg[i], rho[i], rhof[i], rhog[i], x[i], visc[i],viscf[i],viscg[i], \
                                    cond[i],condf[i],condg[i], cp[i],cpf[i],cpg[i], sigma[i], T_wall[i], GRAV, lp[i], rho_s, cp_s[i], k_s[i], T_o, \
					                          check[i], Twet[i], Ton[i], Qcr[i], Tsens[i])
		--	 END PARALLEL

        END EXPAND_BLOCK

        EXPAND_BLOCK(i IN 1, nodes)   
          IF(wallExpOption) INSERT
             dP[i] = (rho[i]' - drho_dh[i] * (u[i]' - P[i]*rho[i]'/rho[i]**2)) / (drho_dp[i] + drho_dh[i]/rho[i])
          ELSE
             dP[i] = 0
          END IF
        END EXPAND_BLOCK

        --  Node spacial derivative & acceleration calculation
        IF (ufm == Damp_def) INSERT
          EXPAND(i IN 1,nodes) k_dd[i] = 1

          EXPAND (i IN 1, nodes)  acc[i] = 0
          EXPAND (i IN 1, nodes)  dvel_dx[i] = 0
          EXPAND (i IN 1, nodes) k_fru[i] = 0
          EXPAND (i IN 1, nodes) zeta_uf[i] = 0

        ELSEIF (ufm == Damp_mod) INSERT 
          EXPAND(i IN 1,nodes) k_dd[i] = L_r/dL[i]/10

          EXPAND (i IN 1, nodes)  acc[i] = 0
          EXPAND (i IN 1, nodes)  dvel_dx[i] = 0
          EXPAND (i IN 1, nodes) k_fru[i] = 0
          EXPAND (i IN 1, nodes) zeta_uf[i] = 0

        ELSEIF (ufm == Brunone AND scheme == centred) INSERT
          EXPAND (i IN 1,nodes) k_dd[i] = 0
        --  Node spacial derivative 
          EXPAND (i IN 1, nodes)  dvel_dx[i] = (m_jun[i+1]-m_jun[i])/dL[i]/rho[i]/A[i]
          --  Node acceleration calculation
          EXPAND (nodes ==1) acc[1] = 0--m_jun[2]'/A[1]/rho[1] - rho[1]'*vel[1]/rho[1]
          EXPAND_BLOCK (nodes >1) 
           acc[1]     = m_jun[2]'    /A[1]/rho[1]         - rho[1]'*vel[1]/rho[1]
           acc[nodes] = m_jun[nodes]'/A[nodes]/rho[nodes] - rho[nodes]'*vel[nodes]/rho[nodes]
          END EXPAND_BLOCK
          EXPAND (i IN 2, nodes-1)  acc[i] = 0.5*(m_jun[i]'+m_jun[i+1]')/A[i]/rho[i] - rho[i]'*vel[i]/rho[i] 

          -- Brunone's Unsteady friction model (unsing Vardy's shear decay coeff.)
          EXPAND (i IN 1, nodes) k_fru[i] = IF (Re[i] > 3000) 2*sqrt(12.86/Re[i]**log10(15.29/Re[i]**0.0567)) ELSE 2*sqrt(0.00476)
          EXPAND (i IN 1, nodes) zeta_uf[i] = k_d*k_fru[i]*2*Dh[i]*(acc[i]-vsound[i]*dvel_dx[i])

        ELSE
          EXPAND (i IN 1,nodes) k_dd[i] = 0
        --  Node spacial derivative 
          EXPAND (i IN 1, nodes)  dvel_dx[i] = (m_jun[i+1]-m_jun[i])/dL[i]/rho[i]/A[i]
        --  Node acceleration calculation
          EXPAND (i IN 1, nodes)  acc[i] = m_cel[i]'/A[i]/rho[i] - rho[i]'*vel[i]/rho[i] 

          -- Brunone's Unsteady friction model (unsing Vardy's shear decay coeff.)
          EXPAND (i IN 1, nodes) k_fru[i] = IF (Re[i] > 3000) 2*sqrt(12.86/Re[i]**log10(15.29/Re[i]**0.0567)) ELSE 2*sqrt(0.00476)
          EXPAND (i IN 1, nodes) zeta_uf[i] = k_d*k_fru[i]*2*Dh[i]*(acc[i]-vsound[i]*dvel_dx[i])

		  END IF

        -------------------------------------------------------------------------------------------------
        -- Centred scheme : donor cell approach, on a staggered grid, with artifical momentum dissipation
        -------------------------------------------------------------------------------------------------
        IF (scheme == centred) INSERT

          -- Numerical viscosity for inlet and outlet port pressure reconstruction & for all nodes in centred scheme
          EXPAND(i IN 1,nodes) qn[i]= Damp *k_d *k_dd[i] * max(abs(vel[i]),vsound[i])* (m_jun[i]-m_jun[i+1])/ A[i]   -- *(1-(1-x[i])*alpha[i])**2

          --  Node velocity calculations
          EXPAND (nodes ==1) vel[1] = 0.5 * (f1.Q - f2.Q) / A[1] / num
          EXPAND_BLOCK (nodes >1) 
            vel[1]     = 0.5*( f1.Q/num + m_jun[2]    /donor_cell(m_jun[2],    rho[1],      rho[2]    ) )/A[1]
            vel[nodes] = 0.5*(-f2.Q/num + m_jun[nodes]/donor_cell(m_jun[nodes],rho[nodes-1],rho[nodes]) )/A[nodes]
          END EXPAND_BLOCK
          EXPAND (i IN 2, nodes-1)  vel[i] = 0.5*(m_jun[i]   / donor_cell(m_jun[i]  ,rho[i-1],rho[i]) + \
                                                  m_jun[i+1] / donor_cell(m_jun[i+1],rho[i]  ,rho[i+1]) ) /A[i]

			 -- Cell-wise mass flows
          EXPAND (i IN 1,nodes) m_cel[i] = 0.5 * (m_jun[i] + m_jun[i+1])

          -- Numerical fluxes
          EXPAND (i IN 1, nodes+1) num_f_mass[i] = m_jun[i]

          num_f_nc[1]       =  f1.m_nc/num
          num_f_nc[nodes+1] = -f2.m_nc/num
          EXPAND (i IN 2, nodes) num_f_nc[i] = m_jun[i] * donor_cell(m_jun[i], x_nc[i-1], x_nc[i])

          num_f_mom[nodes+1] = 0   -- not used in centred scheme
          EXPAND (i IN 1, nodes)  num_f_mom[i] =  (P[i] + rho[i]*vel[i]**2 + qn[i])*A[i]  -- numerical dissipation

          num_f_ene[1]       =  f1.mh/num
          num_f_ene[nodes+1] = -f2.mh/num
          EXPAND (i IN 2, nodes)  num_f_ene[i] = m_jun[i] * donor_cell(m_jun[i], h[i-1], h[i]) + \
			 			                               0.5*(cond[i-1]+cond[i]) * (T[i-1]-T[i]) * (A[i]+A[i-1])/(dL[i-1]+dL[i])

          -- First and last junction mass flows
          m_jun[1]       =  f1.m/num
          m_jun[nodes+1] = -f2.m/num     

          --  the following terms are added, as they are needed for partitioning, 
          --  even if not used in the centred scheme

          source_term_mom[1]  = 0 
          EXPAND_BLOCK (i IN 1,nodes+1)
            s_mass_jun[i]= 0.
--            s_ncd_jun[i]  = 0.
            s_nc_jun[i]  = 0.
            s_mom_jun[i] = 0.
            s_ene_jun[i] = 0.
            s_mass_p[i] = 0.
            s_nc_p[i]   = 0.
--            s_ncd_p[i]  = 0.            
            s_mom_p[i]  = 0.
            s_ene_p[i]  = 0.
            s_mass_m[i] = 0.
--            s_ncd_m[i]  = 0.
            s_nc_m[i]   = 0.
            s_mom_m[i]  = 0.
            s_ene_m[i]  = 0.
				P_AUSM[i] = 0.
          END EXPAND_BLOCK
          EXPAND_BLOCK (i IN 1,nodes)
            s_mass_cel[i] = 0.
            s_nc_cel[i]   = 0.
--            s_ncd_cel[i]  = 0.
            s_mom_cel[i]  = 0.
            s_ene_cel[i]  = 0.
				s_mom_AUSM[i] = 0.
          END EXPAND_BLOCK

			 -- source terms
 
          EXPAND_BLOCK(i IN 2, nodes)  
            source_term_mom[i]  = 0.5 * (P[i-1] + P[i]) * (A[i] - A[i-1]) + \
                                  0.5 * g[i] * (rho[i] * V[i] + rho[i-1] * V[i-1]) - \
                                  0.25 * (zeta[i-1]  * rho[i-1] * vel[i-1] * abs(vel[i-1]) * A[i-1] + \
                                          zeta[i]    * rho[i]   * vel[i]   * abs(vel[i])   * A[i]) - \
                                  0.25 * (zeta_uf[i-1] * rho[i-1] * A[i-1] + \
                                          zeta_uf[i]   * rho[i]   * A[i])

          END EXPAND_BLOCK

          EXPAND_BLOCK(i IN 1, nodes)   
            source_term_mass[i] = -rho[i]*kappa_wall[i]*dP[i]*V[i] 
            source_term_nc[i]   = -rho[i]*kappa_wall[i]*dP[i]*V[i]*x_nc[i]
            source_term_ncd[i]  = 0 -- -rho[i]*kappa_wall[i]*dP[i]*V[i]*xd_nc[i]
            source_term_ene[i]  = -rho[i]*kappa_wall[i]*dP[i]*V[i]*u[i] + q[i] + g[i]*rho[i]*vel[i]*V[i] 
            --  Output signals
            meas_out.signal[1+3*(i-1)] = P[i]
            meas_out.signal[2+3*(i-1)] = T[i]
            meas_out.signal[3+3*(i-1)] = m_jun[i+1]
          END EXPAND_BLOCK  
			  H_gh_0 = 0

        ELSEIF (scheme == Roe) INSERT
        -------------------------------------------------------------------------------------------------
        -- Upwind scheme : full upwind approach on a collocated mesh, using eigenvalue-based amount of dissipation
        -------------------------------------------------------------------------------------------------
          -- Numerical viscosity -- not needed
          EXPAND(i IN 1,nodes) qn[i]= 0

          -- Numerical fluxes in the junctions 1/2 and nodes+1/2 are given by the port variables.
          num_f_mass[1] = f1.m / num
          num_f_nc[1]   = f1.m_nc / num
          num_f_ene[1]  = f1.mh / num
          num_f_mom[1]  = P1*A_in + f1.m*f1.Q/A_in   -- (P1 + f1.rho*f1.v**2)*A_in
          num_f_mass[nodes+1] = -f2.m / num
          num_f_nc[nodes+1]   = -f2.m_nc / num
          num_f_ene[nodes+1]  = -f2.mh / num
          num_f_mom[nodes+1]  = (Pn + f2.rho * f2.v**2) * A_out

          -- Numerical fluxes in the junctions i-1/2, for i = 2,...,nodes, are computed in an external function.
          -- That function does also the source upwinding necessary for variable area geometries

          upwind_Roe_scheme(2,f1.fluid,f1.fluid_nc,fix,mult,eps_lin,order,lim,central,var_rec,dp_cor,rule,\
                            precond,rho_gh_0,rho[1],rho[2],rho[3],vel_gh_0,vel[1],vel[2],vel[3],P_gh_0,P[1],P[2],P[3],\
                            x_nc_gh_0,x_nc[1],x_nc[2],x_nc[3],T[1],T[2],A[1],A[2],ust[1],ust[2],alpha[1],\
                            alpha[2],vsound[1],vsound[2],s_mass_jun[2],s_nc_jun[2],s_mom_jun[2],s_ene_jun[2],\
                            num_f_mass[2],num_f_nc[2],num_f_mom[2],num_f_ene[2],\
                            s_mass_p[2],s_nc_p[2],s_mom_p[2],s_ene_p[2],s_mass_m[2],s_nc_m[2],s_mom_m[2],s_ene_m[2])
  
          EXPAND (i IN 3, nodes-1)  
          upwind_Roe_scheme(i,f1.fluid,f1.fluid_nc,fix,mult,eps_lin,order,lim,central,var_rec,dp_cor,rule,\
                              precond,rho[i-2],rho[i-1],rho[i],rho[i+1],vel[i-2],vel[i-1],vel[i],vel[i+1],\
                              P[i-2],P[i-1],P[i],P[i+1],x_nc[i-2],x_nc[i-1],x_nc[i],x_nc[i+1],T[i-1],T[i],\
                              A[i-1],A[i],ust[i-1],ust[i],alpha[i-1],alpha[i],\
                              vsound[i-1],vsound[i],s_mass_jun[i],s_nc_jun[i],s_mom_jun[i],s_ene_jun[i],\
                              num_f_mass[i],num_f_nc[i],num_f_mom[i],num_f_ene[i],\
                              s_mass_p[i],s_nc_p[i],s_mom_p[i],s_ene_p[i],s_mass_m[i],s_nc_m[i],s_mom_m[i],s_ene_m[i])
 
          upwind_Roe_scheme(nodes,f1.fluid,f1.fluid_nc,fix,mult,eps_lin,order,lim,central,var_rec,dp_cor,\
                            rule,precond,rho[nodes-2],rho[nodes-1],rho[nodes],rho_gh_np1,vel[nodes-2],vel[nodes-1],\
                            vel[nodes],vel_gh_np1,P[nodes-2],P[nodes-1],P[nodes],P_gh_np1,x_nc[nodes-2],x_nc[nodes-1],\
                            x_nc[nodes],x_nc_gh_np1,T[nodes-1],T[nodes],A[nodes-1],A[nodes],\
                            ust[nodes-1],ust[nodes],alpha[nodes-1],alpha[nodes],vsound[nodes-1],vsound[nodes],\
                            s_mass_jun[nodes],s_nc_jun[nodes],s_mom_jun[nodes],s_ene_jun[nodes],\
                            num_f_mass[nodes],num_f_nc[nodes],num_f_mom[nodes],num_f_ene[nodes],\
                            s_mass_p[nodes],s_nc_p[nodes],s_mom_p[nodes],s_ene_p[nodes],\
                            s_mass_m[nodes],s_nc_m[nodes],s_mom_m[nodes],s_ene_m[nodes])

          -- upwind source terms defined in the first and last junctions

          s_mass_p[1] = s_mass_jun[1]
          s_nc_p[1]   = s_nc_jun[1]
--          s_ncd_p[1]   = s_ncd_jun[1]
          s_mom_p[1]  = s_mom_jun[1]
          s_ene_p[1]  = s_ene_jun[1]

          s_mass_m[nodes+1] = s_mass_jun[nodes+1]
          s_nc_m[nodes+1]   = s_nc_jun[nodes+1]
--          s_ncd_m[nodes+1]   = s_ncd_jun[nodes+1]
          s_mom_m[nodes+1]  = s_mom_jun[nodes+1]
          s_ene_m[nodes+1]  = s_ene_jun[nodes+1]

      --  these terms are added as they are needed for partitioning, even if not used

          s_mass_m[1] = 0
--          s_ncd_m[1]  = 0
          s_nc_m[1]   = 0
          s_mom_m[1]  = 0
          s_ene_m[1]  = 0
          s_mass_p[nodes+1] = 0
          s_nc_p[nodes+1]   = 0
--          s_ncd_p[nodes+1]   = 0
          s_mom_p[nodes+1]  = 0
          s_ene_p[nodes+1]  = 0

          EXPAND (i IN 1, nodes+1) m_jun[i] = num_f_mass[i]

           -- Cell-wise mass flows
          EXPAND (i IN 1,nodes) m_cel[i] = rho[i] * vel[i] * A[i]

        -- source terms that do not need special care are evaluated cell-wise (no upwinding necessary)

          EXPAND_BLOCK(i IN 1, nodes)
            s_mass_cel[i] = -            rho[i] * kappa_wall[i] * dP[i] * V[i] -- + mf_in.m_add[i]    -- JP 
--            s_ncd_cel[i]  = - xd_nc[i] * rho[i] * kappa_wall[i] * dP[i] * V[i]            
            s_nc_cel[i]   = -  x_nc[i] * rho[i] * kappa_wall[i] * dP[i] * V[i]
            s_mom_cel[i]  = rho[i]*g[i]*V[i] - 0.5*rho[i]*A[i]*(zeta[i]*vel[i]*abs(vel[i]) + zeta_uf[i])
            s_ene_cel[i]  = rho[i]*g[i]*V[i]*vel[i] + q[i] - rho[i]*kappa_wall[i]*dP[i]*V[i]*u[i]  -- + dQ_add[i] + mf_in.m_add[i]*h[i]   -- JP
				s_mom_AUSM[i] = 0.
           END EXPAND_BLOCK

        -- source terms that need special care are evaluated junction-wise (upwinding necessary)

          EXPAND_BLOCK(i IN 2, nodes)
          s_mass_jun[i]  = 0.
          s_nc_jun[i]   = 0.
--          s_ncd_jun[i]   = 0.
          s_mom_jun[i]  = (P[i] * A[i] + P[i-1] * A[i-1]) / (A[i] + A[i-1]) * (A[i] - A[i-1])
          s_ene_jun[i]  = 0.
			 P_AUSM[i] = 0.
          END EXPAND_BLOCK
          s_mass_jun[1]  = 0.
          s_nc_jun[1]   = 0.
--          s_ncd_jun[1]   = 0.
          s_mom_jun[1]  = (P[1] * A[1] + P_gh_0 * A_gh_0) / (A[1] + A_gh_0)* (A[1] - A_gh_0)
          s_ene_jun[1]  = 0.
			 P_AUSM[1] = 0.

          s_mass_jun[nodes+1] = 0.
          s_nc_jun[nodes+1]   = 0.
--          s_ncd_jun[nodes+1]   = 0.
          s_mom_jun[nodes+1]  = (P_gh_np1 * A_gh_np1 + P[nodes] * A[nodes]) /(A_gh_np1 + A[nodes])* (A_gh_np1 - A[nodes])
          s_ene_jun[nodes+1]  = 0.
			 P_AUSM[nodes+1] = 0.

        -- Summing up source term contributions

          EXPAND_BLOCK (i IN 1, nodes)
            source_term_mass[i] = (s_mass_p[i] + s_mass_m[i+1] + s_mass_cel[i]) 
            source_term_ncd[i]  = 0 -- (s_ncd_p[i]  + s_ncd_m[i+1]  + s_ncd_cel[i] )
            source_term_nc[i]   = (s_nc_p[i]   + s_nc_m[i+1]   + s_nc_cel[i] ) 
            source_term_mom[i]  = (s_mom_p[i]  + s_mom_m[i+1]  + s_mom_cel[i])  
            source_term_ene[i]  = (s_ene_p[i]  + s_ene_m[i+1]  + s_ene_cel[i])  

      --  Output signals
            meas_out.signal[1+3*(i-1)] = P[i]
            meas_out.signal[2+3*(i-1)] = T[i]
            meas_out.signal[3+3*(i-1)] = m_cel[i]
          END EXPAND_BLOCK
			  H_gh_0 = 0

        -----------LEONARDI EDIT  AUSM SCHEME-------BEGIN----------- 
		  ELSEIF (scheme == AUSM) INSERT
        -------------------------------------------------------------------------------------------------
        -- Upwind AUSM scheme : full upwind approach on a collocated mesh, using eigenvalue-based amount of dissipation
        -------------------------------------------------------------------------------------------------
          -- Numerical viscosity -- not needed
          EXPAND(i IN 1,nodes) qn[i]= 0

          -- Numerical fluxes in the junctions 1/2 and nodes+1/2 are given by the port variables.
			 
			 --START COMMENTING HERE IF ALTERNATIVE FIRST INTERFACE IS USED
			 -- FIRST INTERFACE ---
          num_f_mass[1] = f1.m / num
          num_f_nc[1]   = f1.m_nc / num
          num_f_ene[1]  = f1.mh / num
          num_f_mom[1]  = P1*A_in + f1.m*f1.Q/A_in   -- (P1 + f1.rho*f1.v**2)*A_in

           P_AUSM[1] = 0 
			  H_gh_0 = 0
			 --END COMMENTING HERE IF ALTERNATIVE FIRST INTERFACE IS USED
			 
          -- LAST INTERFACE ---
          num_f_mass[nodes+1] = -f2.m / num
          num_f_nc[nodes+1]   = -f2.m_nc / num
          num_f_ene[nodes+1]  = -f2.mh / num
          num_f_mom[nodes+1]  = (Pn + f2.rho * f2.v**2) * A_out
			 
          P_AUSM[nodes+1] = 0  
			 
          /*
			 --***ALTERNATIVE TREATMENT OF FIRST INTERFACE****----
          H_gh_0 = u_gh_0 + 0.5 * (vel_gh_0**2) + P_gh_0/rho_gh_0  --to be changed (valid only for pfgas)					  
			 AUSM_up_HO_less ( 1 , f1.fluid, f1.fluid_nc, lim, order, var_rec, rho_gh_0, rho_gh_0, rho[1], rho[2],\
                                x_nc_gh_0, x_nc_gh_0, x_nc[1], x_nc[2], vel_gh_0, vel_gh_0, vel[1], vel[2],\
                                P_gh_0, P_gh_0, P[1], P[2], vs_gh_0, vsound[1], H_gh_0 , h[2], A_gh_0, A[1],\
                                K_u, K_p, P_AUSM[1], num_f_mass[1],num_f_nc[1],num_f_mom[1],num_f_ene[1])              
          */  
				
              AUSM_up_HO_less ( 2 , f1.fluid, f1.fluid_nc, lim, order, var_rec, rho_gh_0, rho[1], rho[2], rho[3],\
                                x_nc_gh_0, x_nc[1], x_nc[2], x_nc[3], vel_gh_0, vel[1], vel[2], vel[3],\
                                P_gh_0, P[1], P[2], P[3], vsound[1], vsound[2], h[1], h[2], A[1], A[2],\
                                K_u, K_p, P_AUSM[2],num_f_mass[2],num_f_nc[2],num_f_mom[2],num_f_ene[2])          
          
          EXPAND (i IN 3, nodes-1)
              AUSM_up_HO_less ( i , f1.fluid, f1.fluid_nc, lim, order, var_rec, rho[i-2], rho[i-1], rho[i], rho[i+1],\
                                x_nc[i-2], x_nc[i-1], x_nc[i], x_nc[i+1], vel[i-2], vel[i-1], vel[i], vel[i+1],\
                                P[i-2], P[i-1], P[i], P[i+1], vsound[i-1], vsound[i], h[i-1], h[i], A[i-1], A[i],\
                                K_u, K_p, P_AUSM[i],num_f_mass[i],num_f_nc[i],num_f_mom[i],num_f_ene[i])

              AUSM_up_HO_less ( nodes , f1.fluid, f1.fluid_nc, lim, order, var_rec, rho[nodes-2], rho[nodes-1], rho[nodes], rho_gh_np1,\
                                x_nc[nodes-2], x_nc[nodes-1], x_nc[nodes], x_nc_gh_np1, vel[nodes-2], vel[nodes-1], vel[nodes], vel_gh_np1,\
                                P[nodes-2], P[nodes-1], P[nodes], P_gh_np1, vsound[nodes-1], vsound[nodes], h[nodes-1], h[nodes], A[nodes-1], A[nodes],\
                                K_u, K_p, P_AUSM[nodes],num_f_mass[nodes],num_f_nc[nodes],num_f_mom[nodes],num_f_ene[nodes])

          /* 
			 --***ALTERNATIVE TREATMENT OF LAST INTERFACE****----
           H_gh_np1 = u_gh_np1 + 0.5 * (vel_gh_np1**2) + P_gh_np1/rho_gh_np1  --to be checked (only for pfgas)
                        
          AUSM_up_HO_less ( nodes+1 , f1.fluid, f1.fluid_nc, lim, order, var_rec, rho[nodes-1], rho[nodes], rho_gh_np1, rho_gh_np1,\
                                x_nc[nodes-1], x_nc[nodes], x_nc_gh_np1, x_nc_gh_np1, vel[nodes-1], vel[nodes], vel_gh_np1, vel_gh_np1,\
                                P[nodes-1], P[nodes], P_gh_np1, P_gh_np1, vsound[nodes], vs_gh_np1, h[nodes] , H_gh_np1, A[nodes], A_gh_np1,\
                                K_u, K_p,P_AUSM[nodes+1],num_f_mass_AUSM_np1 ,num_f_nc[nodes+1],num_f_mom[nodes+1],num_f_ene_AUSM_np1)
                            
          num_f_mass[nodes+1] =  0.5*((switch_np1+1)*(-f2.m) -(switch_np1-1)*num_f_mass_AUSM_np1)   --ALT_MASS1
          num_f_ene[nodes+1]  =  donor_cell(vel_gh_np1, num_f_ene_AUSM_np1, -f2.mh)  -- JP                       
            */ 
             
          EXPAND_BLOCK (i IN 1,nodes+1)
			 --NOT NEEDED WITH AUSM APPROACH---
            s_mass_jun[i]= 0.
--            s_ncd_jun[i]  = 0.
            s_nc_jun[i]  = 0.
            s_mom_jun[i] = 0.
            s_ene_jun[i] = 0.
            
            s_mass_p[i] = 0.
            s_nc_p[i]   = 0.
--            s_ncd_p[i]  = 0.            
            s_mom_p[i]  = 0.
            s_ene_p[i]  = 0.
            
            s_mass_m[i] = 0.
--            s_ncd_m[i]  = 0.
            s_nc_m[i]   = 0.
            s_mom_m[i]  = 0.
            s_ene_m[i]  = 0.
          END EXPAND_BLOCK
          
          EXPAND (i IN 1, nodes+1) m_jun[i] = num_f_mass[i]

           -- Cell-wise mass flows
          EXPAND (i IN 1,nodes) m_cel[i] = rho[i] * vel[i] * A[i]
          
			 
			 --LIKE IN ROE source terms that do not need special care are evaluated cell-wise (no upwinding necessary)

          EXPAND_BLOCK(i IN 1, nodes)
            s_mass_cel[i] = -            rho[i] * kappa_wall[i] * dP[i] * V[i] -- + mf_in.m_add[i]    -- JP 
--            s_ncd_cel[i]  = - xd_nc[i] * rho[i] * kappa_wall[i] * dP[i] * V[i]            
            s_nc_cel[i]   = -  x_nc[i] * rho[i] * kappa_wall[i] * dP[i] * V[i]
            s_mom_cel[i]  = rho[i]*g[i]*V[i] - 0.5*rho[i]*A[i]*(zeta[i]*vel[i]*abs(vel[i]) + zeta_uf[i])
            s_ene_cel[i]  = rho[i]*g[i]*V[i]*vel[i] + q[i] - rho[i]*kappa_wall[i]*dP[i]*V[i]*u[i] --+ dQ_add[i] + mf_in.m_add[i]*h[i]   -- JP \
            			
          END EXPAND_BLOCK
			 
	 -- Interafce presure is known and differention for the momentum equation is easily done
	 EXPAND(i IN 2,nodes-1)  s_mom_AUSM[i] = (P_AUSM[i]*A_int[i] + P_AUSM[i+1]*A_int[i+1])* (A_int[i+1] - A_int[i]) /( A_int[i] + A_int[i+1])
	 			
	 			--alternative formulation 
	 			-- s_mom_AUSM[1] = (P_AUSM[1]*A_int[1] + P_AUSM[2]*A_int[2])* (A_int[2] - A_int[1]) /( A_int[1] + A_int[2])
	 			-- s_mom_AUSM[nodes] = (P_AUSM[nodes]*A_int[nodes] + P_AUSM[nodes+1]*A_int[nodes+1])* (A_int[nodes+1] - A_int[nodes]) /( A_int[nodes] + A_int[nodes+1])
	 			 
	 			 s_mom_AUSM[1] = (P_gh_0*A_int[1] + P_AUSM[2]*A_int[2])* (A_int[2] - A_int[1]) /( A_int[1] + A_int[2])
	 			 s_mom_AUSM[nodes] = (P_AUSM[nodes]*A_int[nodes] + P_gh_np1*A_int[nodes+1])* (A_int[nodes+1] - A_int[nodes]) /( A_int[nodes] + A_int[nodes+1])


          
-- Summing up source term contributions

          EXPAND_BLOCK (i IN 1, nodes)
            source_term_mass[i] = (s_mass_p[i] + s_mass_m[i+1] + s_mass_cel[i]) 
            source_term_ncd[i]  = 0--(s_ncd_p[i]  + s_ncd_m[i+1]  + s_ncd_cel[i] )
            source_term_nc[i]   = (s_nc_p[i]   + s_nc_m[i+1]   + s_nc_cel[i] ) 
            source_term_mom[i]  = (s_mom_p[i]  + s_mom_m[i+1]  + s_mom_cel[i] + s_mom_AUSM[i])  
            source_term_ene[i]  = (s_ene_p[i]  + s_ene_m[i+1]  + s_ene_cel[i])  

      --  Output signals
            meas_out.signal[1+3*(i-1)] = P[i]
            meas_out.signal[2+3*(i-1)] = T[i]
            meas_out.signal[3+3*(i-1)] = m_cel[i]
          END EXPAND_BLOCK


        -----------LEONARDI EDIT AUSM SCHEME-------END ------- 

        END IF

--     	  Dl_pnts = L_r/(nodes+1)
        -------------------------------------------------------------------------------------------------
        IF(AbsorOption == Active) INSERT
        -------------------------------------------------------------------------------------------------
				--  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				-- For estability reasons, Ra should have the same sign as x_nc, and Rd the same as xd_nc
				--  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           EXPAND_BLOCK (i IN 1, nodes) 
				Ra[i] = IF(x[i] < 1) max(0, 1/tau_a * rho[i]*  x_nc[i]) ELSE 0
				Rd[i] = IF(x[i] < 1) max(0, 1/tau_d * rho[i]* xd_nc[i]) * max(0, Po_nc-P[i])/Po_nc ELSE 0

				-- calculation of the Diffusion coefficient for each cell
				Diff[i] = 0 -- not used
           END EXPAND_BLOCK

--				num_f_diff_liq[1] = 0
--				EXPAND_BLOCK (i IN 2, nodes) 			
--					num_diff_flux(dL[i-1], dL[i], Dl_pnts, A[i-1], A[i], Diff[i-1], Diff[i], xd_nc[i-1], xd_nc[i],rho[i-1], rho[i], num_f_diff_liq[i])
--				END EXPAND_BLOCK
--				num_f_diff_liq[nodes+1] = 0

        ELSEIF(AbsorOption == equilibrium) INSERT   -- PROPERTIES ARE NEEDED for Gas and Liquid (e.g. Temperature)
        -------------------------------------------------------------------------------------------------
				DiffCoeff(UserDefSolubData,Ca,Cd,A_coef_sol,B_coef_sol, f1.fluid_nc, f1.fluid, nodes, TI, Diff_Turb_Factor, 
			 				P, vel, x_nc, xd_nc, rho, rhof, T, P_nc, viscf,     Ra, Rd, Diff) --, Diff_gas) 

--				num_f_diff_liq[1] = 0
--				EXPAND_BLOCK (i IN 2, nodes) 			
--					num_diff_flux(dL[i-1], dL[i], Dl_pnts, A[i-1], A[i], Diff[i-1], Diff[i], xd_nc[i-1], xd_nc[i],rho[i-1], rho[i], num_f_diff_liq[i])
--				END EXPAND_BLOCK
--				num_f_diff_liq[nodes+1] = 0
				

        ELSE
           EXPAND_BLOCK (i IN 1, nodes) 
			     Ra[i] = 0
			     Rd[i] = 0
			     Diff[i] = 0
--			     num_f_diff_liq[i] = 0 
           END EXPAND_BLOCK
--           num_f_diff_liq[nodes+1] =0
        END IF

        -------------------------------------------------------------------------------------------------
        --Diluted gas mass flow calculation (only centred scheme)
        -------------------------------------------------------------------------------------------------

        md_nc_jun[1]       =  f1.md_nc/num
        md_nc_jun[nodes+1] = -f2.md_nc/num
        EXPAND (i IN 2, nodes) md_nc_jun[i] = m_jun[i] * donor_cell(m_jun[i],xd_nc[i-1],xd_nc[i])

        --Chemicals mass flow calculation
        EXPAND_BLOCK( j IN burnerGasesOption)
        m_nx_jun[j,1]       =  f1.m_nx[j]/num
        m_nx_jun[j,nodes+1] = -f2.m_nx[j]/num
        EXPAND (i IN 2, nodes) m_nx_jun[j,i]  = IF(ChemCode[f1.fluid] >= 0) 0  ELSE m_jun[i]*donor_cell(m_jun[i],x_chem[j,i-1],x_chem[j,i])
        END EXPAND_BLOCK

        -------------------------------------------------------------------------------------------------
        -- Governing equations
        -------------------------------------------------------------------------------------------------

        EXPAND_BLOCK (i IN 1, nodes)
          
          --  Conservation of total mass
          rho[i]' * V[i] = num_f_mass[i] - num_f_mass[i+1] + source_term_mass[i] 

          --  Conservation of non-condensable mass including Absorption/desorption mass flow calculation
          IF(AbsorOption == noActive) INSERT
             (rho[i] * x_nc[i]' + x_nc[i] *rho[i]')* V[i] = IF(ChemCode[f1.fluid] < 0) 0  ELSE  num_f_nc[i] - num_f_nc[i+1]  + source_term_nc[i]
             (rho[i] *xd_nc[i]' + xd_nc[i]*rho[i]')* V[i] = IF(ChemCode[f1.fluid] < 0) 0  ELSE  md_nc_jun[i]- md_nc_jun[i+1] + source_term_ncd[i]
          ELSE
             (rho[i] * x_nc[i]' + x_nc[i] *rho[i]')* V[i] = IF(ChemCode[f1.fluid] < 0) 0  ELSE  num_f_nc[i] - num_f_nc[i+1]  + source_term_nc[i] \
				 																										- V[i]*(Ra[i]-Rd[i])
             (rho[i] *xd_nc[i]' + xd_nc[i]*rho[i]')* V[i] = IF(ChemCode[f1.fluid] < 0) 0	ELSE  md_nc_jun[i]- md_nc_jun[i+1] + source_term_ncd[i] \
				 																										+ V[i]*(Ra[i]-Rd[i]) -- + num_f_diff_liq[i] - num_f_diff_liq[i+1]
          END IF

          --Chemicals mass fractions calculation according to a homogeneous mixture
          EXPAND_BLOCK(j IN burnerGasesOption)
             V[i]*rho[i]*x_chem[j,i]' = IF(ChemCode[f1.fluid] >= 0) 0  
                                        ELSE m_nx_jun[j,i] - m_nx_jun[j,i+1] - x_chem[j,i]*V[i]*rho[i]'
          END EXPAND_BLOCK

          --  Conservation of energy
          (rho[i]' * u[i]  +  rho[i] * u[i]') * V[i] = num_f_ene[i]  - num_f_ene[i+1] + source_term_ene[i]

        END EXPAND_BLOCK

        --  Conservation of momentum
         IF (scheme == centred) INSERT
           EXPAND (i IN 2, nodes) 0.5 * (dL[i-1] + dL[i]) * m_jun[i]' = num_f_mom[i-1] - num_f_mom[i] + source_term_mom[i]
        ELSE
           EXPAND (i IN 1, nodes)  dL[i] * m_cel[i]' = num_f_mom[i] - num_f_mom[i+1] + source_term_mom[i]
		  END IF
		  
		  -- Mass within the pipe
		  	mass 		= SUM (i IN 1,nodes; rho[i]*V[i])
			mass_g 	= SUM (i IN 1,nodes; x[i]*rho[i]*V[i])
			mass_l	= SUM (i IN 1,nodes; (1-x[i])*rho[i]*V[i])
		  
        -------------------------------------------------------------------------------------------------
        -- auxiliary variables not depending on the node number
        -------------------------------------------------------------------------------------------------
        T1 = T[1]
        Tn = T[nodes]
        P1 = f1.P - qn[1]
        Pn = f2.P - qn[nodes]
        alpha1 = alpha[1]
        alphan = alpha[nodes]
        vsound1 = vsound[1]
        vsoundn = vsound[nodes]
        vel1 =vel[1]
        veln =vel[nodes]
        m1 =m_cel[1]
        mn =m_cel[nodes]
        rho1 =rho[1]
        rhon =rho[nodes]
        arhovel1 = vsound1 * rho1 * vel1
        arhoveln = vsoundn * rhon * veln
        inertance_node =SUM (i IN 1, nodes; dL[i]/A[i]) / nodes --Mean Inertance per node dL/A

        dP_loss = P1 - Pn
END COMPONENT

---------------------

COMPONENT Tube_Chilldown IS_A ABS_Tube_Chilldown \
"Cylindrical area-varying non-uniform mesh high resolved 1D fluid vein with 1D heat transfer with the wall"

    PORTS
        IN  thermal(n = nodes) tp_in    "Thermal port connected to the inner wet pipe wall"

    DATA
        REAL L = 1.0				UNITS u_m		"Pipe length -also used to normalise 'D_vs_L' table"
        REAL D = 0.01			UNITS u_m		"Pipe inner diameter -also used to normalise 'D_vs_L' table"
        TABLE_1D D_vs_L  = {{0,0.5,1},{1,1,1}}  \
                              UNITS no_units	"Dimensionless diameter vs dimensionless axial position"
        TABLE_1D dx_vs_L = {{0,0.5,1},{1,1,1}}  \
                              UNITS no_units	"Weighting function for mesh size distribution"
    DECLS
        DISCR REAL SR_bend[n_bends]=1	UNITS no_units	"Side ratio of bend cross section"
        REAL A_wet[nodes]		UNITS u_m2		"Cell wetted area array"
        DISCR REAL D_old, L_old
        CLOSE wallExpOption = FALSE

    INIT
        D_old = D
		  L_old = L
        -- geometrical mesh construction
        mesh_size_circ(nodes, L, D, D_vs_L, dx_vs_L, V, Dh, A, A_wet)
        A_in = 0.25*PI*(D*linearInterp1D(D_vs_L, 0))**2
        A_out= 0.25*PI*(D*linearInterp1D(D_vs_L, 1))**2
        FOR (i  IN 1,nodes)
           u[i]   = u[i] + 0.5 * (m_o/num/rho[i]/A[i])**2
        END FOR
		  IF (ht_option_FB == HT_darr2015) THEN
		  	
		  	ASSERT(L/D<100) WARNING "Tube L/D is outside Darr 2015 range"
			END IF
    CONTINUOUS
        -- geometrical mesh construction
		  SEQUENTIAL
		  IF(D != D_old OR L!=L_old) THEN
        D_old = D
		  L_old = L
        mesh_size_circ(nodes, L, D, D_vs_L, dx_vs_L, V, Dh, A, A_wet)
        A_in = 0.25*PI*(D*linearInterp1D(D_vs_L, 0))**2
        A_out= 0.25*PI*(D*linearInterp1D(D_vs_L, 1))**2
		  END IF
		  END SEQUENTIAL

        EXPAND (i IN 1,nodes)  dL[i] = V[i] / A[i]

        zeta_bends =  SUM(j IN 1, n_bends; hdc_k_bend(Circular, alpha_bend[j], \
                                                      R_bend[j]/Dh[1], rug/Dh[1], SR_bend[j], 1e5))

        --  Heat transfer (one port)

        EXPAND (i IN 1, nodes)   q[i] = hc[i] * A_wet[i] * (tp_in.Tk[i]-T[i])
        EXPAND (i IN 1, nodes)   tp_in.q[i] = num * q[i]
        EXPAND (i IN 1, nodes)   kappa_wall[i] = 0
        EXPAND (i IN 1, nodes)   T_wall[i] = tp_in.Tk[i]
END COMPONENT
 
 




COMPONENT Tube_Annular_Chilldown IS_A ABS_Tube_Chilldown \
"Annular high resolved 1D fluid vein exchanging heat with two thermal ports"

    PORTS
        IN  thermal(n = nodes)  tp_in    "Thermal port connected to the inner wet pipe wall"
        OUT thermal(n = nodes)  tp_out   "Thermal port connected to the external wet pipe wall"

    DATA
        REAL L = 1.0				UNITS u_m		"Pipe length"
        REAL A_cr = 1e-4		UNITS u_m2		"Cross-section flow area"
        REAL Pw_in = 0.010		UNITS u_m		"Inner wet perimeter"
        REAL Pw_out= 0.010		UNITS u_m		"Outer wet perimeter"

    DECLS
        DISCR REAL SR_bend[n_bends]= 1		UNITS no_units	"Side ratio of bend cross section"
--        CLOSE burnerGasesOption = noBurnGases
        CLOSE wallExpOption = FALSE

    INIT
        FOR (i  IN 1,nodes)
           dL[i] = L / nodes
           Dh[i] = 4*A_cr/(Pw_in + Pw_out)
           A[i]  = A_cr
           V[i]  = A[i] * dL[i]
           u[i]   = u[i] + 0.5 * (m_o/num/rho[i]/A[i])**2
        END FOR

    CONTINUOUS
        A_in  = A_cr
        A_out = A_cr
        EXPAND_BLOCK (i  IN 1,nodes)
          dL[i] = L / nodes
          Dh[i] = 4*A_cr/(Pw_in + Pw_out)
          A[i]  = A_cr
          V[i]  = A[i] * dL[i]
        END EXPAND_BLOCK

        zeta_bends =  SUM(j IN 1, n_bends; hdc_k_bend(Circular, alpha_bend[j],\
                                                      R_bend[j]/Dh[1], rug/Dh[1], SR_bend[j], 1e5))
        --  Heat transfer (two ports)

        EXPAND (i IN 1, nodes)   q[i] = tp_in.q[i]/num - tp_out.q[i]/num

        EXPAND (i IN 1, nodes)   tp_in.q[i]  = num * hc[i]*dL[i] * Pw_in *(tp_in.Tk[i] -T[i])
        EXPAND (i IN 1, nodes)   tp_out.q[i] =-num * hc[i]*dL[i] * Pw_out*(tp_out.Tk[i]-T[i])
        EXPAND (i IN 1, nodes)   kappa_wall[i] = 0
        EXPAND (i IN 1, nodes)   T_wall[i] = tp_in.Tk[i]
END COMPONENT


COMPONENT Tube_HP_Chilldown IS_A ABS_Tube_Chilldown \
"Heat pipe type non-uniform mesh high resolved 1D fluid vein exchanging heat with a thermal port"

    PORTS
        IN  thermal(n = nodes) tp_in    "Thermal port connected to the inner wet pipe wall"

    DATA
        REAL L = 1.0				UNITS u_m		"Pipe length"
        REAL A_cr = 1e-4		UNITS u_m2		"Cross-section flow area"
        REAL Pw_in = 0.010		UNITS u_m		"Inner wet perimeter"

    DECLS
        DISCR REAL SR_bend[n_bends]= 1		UNITS no_units	"Side ratio of bend cross section"
        CLOSE burnerGasesOption = noBurnGases
        CLOSE wallExpOption = FALSE

    INIT

    CONTINUOUS
        A_in  = A_cr
        A_out = A_cr
        EXPAND_BLOCK (i  IN 1,nodes)
          dL[i] = L / nodes
          Dh[i] = 4*A_cr/Pw_in
          A[i]  = A_cr
          V[i]  = A[i] * dL[i]
        END EXPAND_BLOCK

        zeta_bends =  SUM(j IN 1, n_bends; hdc_k_bend(Circular, alpha_bend[j],\
                                                      R_bend[j]/Dh[1], rug/Dh[1], SR_bend[j], 1e5))
        --  Heat transfer (one port)

        EXPAND (i IN 1, nodes)   q[i] = hc[i]*dL[i] * Pw_in *(tp_in.Tk[i] -T[i])
        EXPAND (i IN 1, nodes)   tp_in.q[i]  = num * q[i]

        EXPAND (i IN 1, nodes)   kappa_wall[i] = 0
        EXPAND (i IN 1, nodes)   T_wall[i] = tp_in.Tk[i]
END COMPONENT



COMPONENT Tube_Rect_Chilldown IS_A ABS_Tube_Chilldown \
"Rectangular area-varying non-uniform mesh high resolved 1D fluid vein exchanging heat with 3 thermal ports"

    PORTS
        IN  thermal(n = nodes)  tp_in    "Thermal port connected to the inner wet pipe wall"
        OUT thermal(n = nodes)  tp_out   "Thermal port connected to the external wet pipe wall"
        IN  thermal(n = nodes)  tp_lat   "Thermal port connected to the lateral wet pipe walls, both sides"

    DATA
        REAL L = 1.0				UNITS u_m		"Pipe length -also used to normalise profile tables"
        REAL a = 0.01			UNITS u_m		"Cross section width -also used to normalise profile tables"
        REAL b = 0.01			UNITS u_m		"Cross section height -also used to normalise profile tables"
        TABLE_1D a_vs_L  = {{0,0.5,1},{1,1,1}}  \
                              UNITS no_units	"Dimensionless cross section width vs dimensionless axial position"
        TABLE_1D b_vs_L  = {{0,0.5,1},{1,1,1}}  \
                              UNITS no_units	"Dimensionless cross section height vs dimensionless axial position"
        TABLE_1D dx_vs_L = {{0,0.5,1},{1,1,1}}  \
                              UNITS no_units	"Weighting function for mesh size distribution"
        REAL SR_bend[n_bends]= 1	UNITS no_units	"Side ratio of bend cross section"

    DECLS
        REAL av[nodes]		UNITS u_m		"Cross section widths"
        REAL bv[nodes]		UNITS u_m		"Cross section heights"
--         CLOSE burnerGasesOption = noBurnGases
        CLOSE wallExpOption = FALSE
        DISCR REAL a_old, b_old, L_old

    INIT
        a_old = a
        b_old = b
		  L_old = L
        -- geometrical mesh construction
        mesh_size_rect(nodes, L, a, b, a_vs_L, b_vs_L, dx_vs_L, V, Dh, A, av, bv)
        A_in  = a*linearInterp1D(a_vs_L,0) * b*linearInterp1D(b_vs_L,0)
        A_out = a*linearInterp1D(a_vs_L,1) * b*linearInterp1D(b_vs_L,1)
        FOR (i  IN 1,nodes)
           u[i]   = u[i] + 0.5 * (m_o/num/rho[i]/A[i])**2
        END FOR

    CONTINUOUS
        -- geometrical mesh construction
		  SEQUENTIAL
		  IF(a != a_old OR b != b_old OR L!=L_old) THEN
        mesh_size_rect(nodes, L, a, b, a_vs_L, b_vs_L, dx_vs_L, V, Dh, A, av, bv)
        A_in  = a*linearInterp1D(a_vs_L,0) * b*linearInterp1D(b_vs_L,0)
        A_out = a*linearInterp1D(a_vs_L,1) * b*linearInterp1D(b_vs_L,1)
		  END IF
		  END SEQUENTIAL

        EXPAND (i IN 1,nodes)  dL[i] = V[i] / A[i]

        zeta_bends =  SUM(j IN 1, n_bends; hdc_k_bend(Rectangular, alpha_bend[j],\
                                                      R_bend[j]/Dh[1], rug/Dh[1], SR_bend[j], 1e5))

        --  Heat transfer (3 ports)

        EXPAND (i IN 1, nodes)   q[i] = tp_in.q[i]/num + tp_lat.q[i]/num - tp_out.q[i]/num

        EXPAND (i IN 1, nodes)   tp_in.q[i]  =  num * hc[i] * dL[i] * av[i]*(tp_in.Tk[i] -T[i])
        EXPAND (i IN 1, nodes)   tp_lat.q[i] =2*num * hc[i] * dL[i] * bv[i]*(tp_lat.Tk[i]-T[i])
        EXPAND (i IN 1, nodes)   tp_out.q[i] = -num * hc[i] * dL[i] * av[i]*(tp_out.Tk[i]-T[i])

        EXPAND (i IN 1, nodes)   kappa_wall[i] = 0
        EXPAND (i IN 1, nodes)   T_wall[i] = tp_in.Tk[i]
END COMPONENT
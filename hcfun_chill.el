/*-----------------------------------------------------------------------------------------
 LIBRARY: CHILLDOWN_2
 FILE: hcfun_chill
 Authors: M.F., G.G., B.L., S.D. - Sapienza University of Rome
 CREATION DATE: 03/10/2022
-----------------------------------------------------------------------------------------*/

USE MATH
USE FLUID_PROPERTIES
USE THERMO_TABLE_INTERP

ENUM HT_OPT_CHILL_FB  =  {HT_const_FB, HT_darr2015,HT_darr2016,HT_darr2019,HT_mei,HT_bromley,HT_miropolskii,HT_groeneveld,HT_bromleymod}      "Heat Transfer option - Film boiling model"
ENUM HT_OPT_CHILL_TW  =  {HT_const_TW, HT_darr2015_TW, HT_kalinin_TW, HT_darr2019_TW}      "Heat Transfer option - Rewet temperature model"
ENUM HT_OPT_CHILL_TB  =  {HT_const_TB, HT_darr2015_TB}      "Heat Transfer option - Transition boiling model"
ENUM HT_OPT_CHILL_NB  =  {HT_const_NB, HT_Chen}    			  "Heat Transfer option - Nucleate boiling model"
ENUM HT_OPT_CHILL_Ton =  {HT_const_Ton, HT_darr2015_Ton, HT_Tsat_Ton, HT_frost_Ton} "Heat Transfer option - Nucleate boiling onset model"
ENUM HT_OPT_CHILL     =  {HT_auto, HT_dat ,HT_TUBE}
ENUM HT_OPT_CHILL_Qcr =  {HT_const_qcr, HT_darr2015_qcr} "Heat transfer option - Critical heat flux" 



-- Function computes the critical temperature
FUNCTION REAL Tcr_comp
(
	IN ENUM FluidKeys fluid		"Working fluid"
)

DECLS
 	REAL Tcrit
 	INTEGER ig
	INTEGER ier
	REAL prop_crit[20]			  "Fluid properties at critical point"

BODY
  FL_table_read(fluid, ig)
	crit_cond  (ig, prop_crit, ier)
	Tcrit = prop_crit[2]
 
RETURN Tcrit
END FUNCTION



-- Function computes the gas phase viscosity as a function of pressure and temperature
FUNCTION REAL visc_comp
(
  IN ENUM FluidKeys fluid	           "Working fluid",
	IN REAL T						               "Temperature",
	IN REAL p						               "pressure"
)

DECLS
	REAL prop[3,20] 				           "properties"
	REAL xfic      				             "quality"
	INTEGER ier1   				             "error flag"
	INTEGER INTERP_ORDER = 1           "Interpolation order"
	INTEGER ipx,ipy 				           "pressure and temperature pointers in table"
	INTEGER ig 						             "fluid identification number"
	REAL visc1, visc2, visc3   
	REAL viscg

BODY
	-- find fluid identification number
	FL_table_read(fluid, ig)
	-- read thermo tables
	thermo_prop(ig, 1, p, 2, T, 111, prop, xfic, ier1, INTERP_ORDER, ipx, ipy)
	-- viscosities of fluid, saturated liquid and saturated vapour
	viscg  = prop[1,20]
	visc2  = prop[2,20]
	visc3  = prop[3,20]
	-- if two-phase, use viscosity of saturated vapour
	IF (xfic<1) THEN
		viscg = visc3
	END IF

RETURN viscg
END FUNCTION



-- Function computes the pool boiling heat transfer coefficient
FUNCTION REAL htc_fun_pool
(
	IN REAL m						UNITS u_kg_s	 "Average mass flow through volume (kg/s)",
  IN REAL A						UNITS u_m2		 "Cross area (m^2)",
  IN REAL Dh					UNITS u_m		   "Hydraulic diameter (m)",
  --fluid properties
  IN REAL p						UNITS u_Pa		 "pressure (Pa)",
  IN REAL T						UNITS u_K		   "fluid temperature (K)",
  IN REAL Tsat				UNITS u_K		   "saturation temperature (K)",
	IN REAL Tcr					UNITS u_K      "Critical temperature (K)",
  IN REAL hf					UNITS u_J_kg	 "Enthalpy of saturated liquid (J/kg)",
  IN REAL hg					UNITS u_J_kg	 "Enthalpy of saturated vapor  (J/kg)",
  IN REAL rho					UNITS u_kg_m3	 "fluid density (kg/m^3)",
  IN REAL rhof				UNITS u_kg_m3	 "density of liquid (kg/m^3)",
  IN REAL rhog				UNITS u_kg_m3	 "density of vapor (kg/m^3)",
  IN REAL x						UNITS no_units "quality (-)",
  IN REAL visc				UNITS u_Pas		 "viscosity  (Pa*s)",
  IN REAL viscf				UNITS u_Pas		 "viscosity of liquid (Pa*s)",
  IN REAL viscg				UNITS u_Pas		 "viscosity of vapor (Pa*s)",
  IN REAL k						UNITS u_W_mK	 "thermal conductivity  (W/m*K)",
  IN REAL kf					UNITS u_W_mK	 "thermal conductivity of liquid (W/m*K)",
  IN REAL kg					UNITS u_W_mK	 "thermal conductivity of vapor  (W/m*K)",
  IN REAL cp					UNITS u_J_kgK	 "specific heat (J/kg*K)",
  IN REAL cpf					UNITS u_J_kgK	 "specific heat of liquid (J/kg*K)",
  IN REAL cpg					UNITS u_J_kgK	 "specific heat of vapor (J/kg*K)",
  --wall properties
  IN REAL sigma				UNITS u_N_m		 "Surface tension (N/m)",
  IN REAL Tw					UNITS u_K		   "wall temperature (K)",
  IN REAL Grav				UNITS u_m_s2   "Gravitational acceleration (m/s^2)"
)

DECLS
	REAL htc	
	REAL dh,dT,drho,L_b,Pr_l,Pr_v
	REAL Ra,Ja,Ph
	REAL q_ch,dT_ch
	REAL K, q_cr,T_cr
	REAL q_mi,Nu_fb,T_fb
	        
  REAL Csf = 0.015	UNITS no_units	"Adjusting constant for nucleate boiling (-)"
  REAL r = 0.4		  UNITS no_units	"Adjusting exponent for nucleate boiling (-)"
	
BODY
	IF(T>=Tsat OR Tw<=Tsat OR T>Tsat OR sigma<1e-6) THEN      -- No boiling
   	htc = htc_tube_sp(m, A, Dh, k, visc, cp)
  ELSE
    dh = max(1e-3, hg - hf)
    dT = Tw - Tsat
    drho = rhof - rhog

		--  L_b  = sqrt(sigma/Grav/drho)           -- capillary length
		L_b  = max( 1e-6, 1.8*sqrt(sigma/max(1e-4, Grav*rhof)) )
		Pr_l = cpf * viscf / kf
		Pr_v = cpg * viscg / kg
	
		Ra = abs(Grav * rhog * drho) * Dh**3 * Pr_v / viscg**2
		Ja = cpg * abs(dT) / dh
		Ph = cpf *(Tsat-T) / dh               -- Phase nb.
		
		q_ch  = viscf*dh/L_b                   -- Characteristic capillary flux
		dT_ch = Csf*dh/cpf*Pr_l**1.7           -- Characteristic dT
	
		-- Maximum heat flux
		K = 0.16*(1 + Ph*0.065*(rhof/rhog)**0.8)   -- Kutateladze correlation
		q_cr = K*dh*sqrt(rhog)*(sigma*drho*Grav)**0.25
		T_cr = Tsat + dT_ch * (q_cr/q_ch)**r
		
		-- Minimum heat flux
		q_mi = 0.09*dh*rhog*(sigma*drho*Grav/(rhof+rhog)**2)**0.25   -- Zuber correlation
		Nu_fb = 0.425*(Dh/L_b*Ra/Ja)**0.25
		T_fb  = T_cr + q_mi*Dh/kg/Nu_fb
		--  PRINT("     T_cr=$T_cr T_fb=$T_fb Pr_l=$Pr_l q_cr=$q_cr q_mi=$q_mi q_ch=$q_ch K=$K Ph=$Ph dh=$dh dT_ch=$dT_ch")
	
		IF(Tw < T_cr) THEN        -- Nucleate boiling
			htc = q_ch *fpow(dT/dT_ch, 0.001, 1/r) / abs(T-Tw)
		ELSE
		  ASSERT(T_fb > T_cr) FATAL "The film boiling temperature is lower than Tcrit_heat_flux"

	  	IF(Tw > T_fb) THEN        -- Film boiling
	    	htc = kg/Dh * Nu_fb * dT / abs(T-Tw)
	  	ELSE                      -- Transition boiling
	    	htc = (q_cr + (q_mi-q_cr)/(T_fb-T_cr)*(Tw-T_cr)) / abs(T-Tw)
  		END IF
  	
		END IF
	
	END IF
	
RETURN htc
END FUNCTION
 


-- Function computes the forced film boiling heat transfer coefficient
FUNCTION REAL htc_fun_FB
(
	IN ENUM HT_OPT_CHILL_FB option_FB	      	"Heat transfer option - chilldown - Film boiling model",
	IN REAL htc_FB_dat				UNITS u_W_m2K		"Data for the heat transfer coefficient if ht_option_FB = ht_const_FB (W/m^2*K)",
	IN REAL m					        UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A						      UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh					      UNITS u_m		  	"Hydraulic diameter (m)",
	 --fluid properties
	IN REAL p									UNITS u_Pa			"pressure (Pa)",
	IN REAL T									UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat							UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr								UNITS u_K     	"Critical temperature (K)",
	IN REAL hf								UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg								UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho								UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof							UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog							UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x									UNITS no_units	"quality (-)",
	IN REAL visc							UNITS u_Pas		  "viscosity  (Pa*s)",
	IN REAL viscf							UNITS u_Pas		  "viscosity of liquid (Pa*s)",
	IN REAL viscg							UNITS u_Pas		  "viscosity of vapor (Pa*s)",
	IN REAL k									UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf								UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg								UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp								UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf								UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg								UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	--wall properties
	IN REAL sigma							UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Tw								UNITS u_K				"wall temperature (K)",
	IN REAL Grav							UNITS u_m_s2		"Gravitational acceleration (m/s^2)",
	IN REAL xwall							UNITS u_m   		"distance from inlet",
	IN REAL Twet							UNITS u_K     	"Rewet temperature",
	IN REAL T0								UNITS u_K				"Initial wall temperature",
	OUT REAL flag							UNITS no_units
)

DECLS
	REAL glv
	REAL htcFB
	REAL c[9]
	REAL Ja,Ra,We,Reg,Prg
	REAL h1, h2, h3, Nufb
	REAL viscg_wall
	REAL arg, erfc_a, erf
	REAL theta, dT, drho
	REAL al,fi0,G
	REAL Lh
	REAL Remix, Y
	TABLE_1D erfc_table = {{-100,-4,-3,-2.5,-2,-1.5,-1,0,1,1.5,2,2.5,3,4,100},{2,1.99999998458274,1.99997790950300,1.99959304798256,1.99532226501895,1.96610514647531,1.84270079294972,1,0.157299207050285,0.0338948535246893,0.00467773498104727,0.000406952017444959,2.20904969985854e-05,1.54172579002800e-08,0}}
	TABLE_1D x_vs_fi0 ={{0,0.02638,0.1663,0.4496,0.7397,0.9345,1},{0,0.5077,0.9837,1.491,1.967,2.443,3.14}}
	REAL htc1ph
	
BODY
	glv = hg-hf -- latent heat of fusion
	
	-- dat		
	IF (option_FB == HT_const_FB) THEN
		htcFB = htc_FB_dat
	
	-- S. Darr et al., The effect of reduced gravity on cryogenic nitrogen boiling and pipe chilldown, 2016
	ELSEIF (option_FB == HT_darr2016) THEN
		
		IF (Grav < 1) THEN
		 	c[1] = 0.15
			c[2] = -0.76
			c[3] = 0.52
			c[4] = 5e-6
			c[5] = 0.1
			c[6] = 2
			c[7] = 0
		ELSE
		 	c[1] = 0.112
			c[2] = -0.8
			c[3] = 0.57
			c[4] = 7E-5
			c[5] = 0.45
			c[6] = 2
			c[7] = -0.25	
		END IF
		
		Ja = cpf*max((Tw-Tsat),2)/(hg-hf)
		We = (m/A)**2*Dh/(rhof*sigma)
		Reg = abs(m/A)*Dh/viscg
		Prg = viscg*cpg/kg
		h1 = c[1]*(1+(Dh/(xwall+0.1*Dh))**0.7)*(kg/Dh)*(1-min(x,0.4))**c[2]*Reg**c[3]*Prg**0.4 
		h2 = c[4]*(kf/Dh)*exp(-(xwall/Dh)**c[5])*We**c[6]*Ja**(-1)
		h3 = c[7]*(rhog*(rhof-rhog)*Grav*(hg-hf+0.375*cpg*max((Tw-Tsat),2))*kg**3/(4*xwall*viscg*max((Tw-Tsat),2)))**0.25
		htcFB = h1 + h2 + h3
				
	-- S. Darr et al., Two-Phase Pipe Quenching Correlations for Liquid Nitrogen and Liquid Hydrogen, 2019
	ELSEIF (option_FB == HT_darr2019) THEN
		
		IF (Grav < 1) THEN
		 	c[1] = 0.032
			c[7] = 0.1
		ELSE
		 	c[1] = 0.044 
			c[7] = 0
		END IF

		c[2] = 0.4
		c[3] = 0.7
		c[4] = 0.9
		c[5] = 5e3
		c[6] = 2
		c[8] = 5e-5
		c[9] = 0.5
		We = (m/A)**2*Dh/(rhof*sigma)
		Ja = (hg-hf)/(cpg*max((Tw-Tsat),2))
		Prg = viscg*cpg/kg
		viscg_wall = viscg -- for the sake of simplicity
		Reg = abs(m/A)*Dh/viscg
		arg = (x+1)/(log(abs(Reg))/log(c[5]))
		-- aproximate formula for erfc computation, taken from  https://doi.org/10.1002/asl.154
		erfc_a = 2.7749
		IF (arg>0) THEN
			erf = 1-erfc_a/((erfc_a-1)*sqrt(PI*arg**2)+sqrt(PI*arg**2+erfc_a**2))/exp(arg**2)
		ELSE
			erf = -1+erfc_a/((erfc_a-1)*sqrt(PI*arg**2)+sqrt(PI*arg**2+erfc_a**2))/exp(arg**2)
		END IF
		Nufb = c[1]*(1+(1/(xwall/Dh+0.1))**c[2])*Reg**c[3]*Prg**c[4]*(erf)**c[6]*(viscg/viscg_wall)**c[7]+c[8]*(kf/kg)*exp(-(xwall/Dh)**c[9])*We**2*Ja
		htcFB = Nufb*kg/Dh
				
	-- S. Darr et al., Numerical Simulation of the Liquid Nitrogen Chilldown of a Vertical Tube, 2015 DOI:10.2514/6.2015-0468		
	ELSEIF (option_FB == HT_darr2015) THEN

 		theta = (T0-Tw)/(T0-Twet)				
		We = (m/A)**2*Dh/(rhof*sigma) -- unclear if Wez o WeD
		Reg = abs(m/A)*Dh/viscg
		Prg = viscg*cpg/kg

		-- xwall distance from inlet
		-- x is limited to 0.4 to avoid singularity. Correlation is derived with xeq<0.4
 		Nufb = (7.55e-4 - 7.43e-6*xwall/Dh)*Reg**(0.941)*(1-min(x,0.4))**(-5.23)*Prg**(0.4)+0.0568*(kf/kg)*We*theta**3
		htcFB = Nufb*kg/Dh	
	
	-- Mei, J. liao and R. Mei, A film boiling model for cryogenic chilldown at low mass flux inside a horizontal pipeline, 2006
	ELSEIF (option_FB == HT_mei) THEN

		dT = Tw - Tsat
		drho = rhof - rhog
		Ra = Grav*Dh**3*drho/(viscg/rhog*kg/(rhog*cpg)*rhog)
		Ja = cpg * abs(dT) / glv
		Prg = cpg*viscg/kg 
		al=(1-x)*rhog/(x*rhof+(1-x)*rhog)
		fi0 = max(PI,interp1D(x_vs_fi0,LINEAR,CONSTANT,al))
		G = fi0-1/60*fi0**3-1/9600*fi0**5-6.5133e-6*fi0**7
		Nufb = 0.2034*(Ra/Ja)**(1/4)*G
		h2 = 0--0.023*(m*x/A*Dh/viscg)**0.8*Prg**0.4 -- m.f. use of gas-phas hydraulic diameter lead to singularity
		h1 = kg*Nufb/Dh
		htcFB = h1--(htc1*fi0+(PI-fi0)*htc2)/PI		
	
	-- L. A. Bromley et al., Heat Transfer in Forced Convection Film Boiling 1953
	ELSEIF (option_FB == HT_bromley) THEN
	 	dT = abs(Tw-Tsat)
		Lh = min(Dh/2,2*PI*sqrt(glv/(Grav*(rhof-rhog))))
		htcFB = 0.62*(kg**3*rhog*Grav*(rhof-rhog)*(glv+0.4*cpg*dT)/(Lh*viscg*dT))**(1/4)

	-- Miropolskii, K.R. Kunniyor and P. Ghosh, Development of transient flow film boiling heat transfer correlations
-- for energy efficient cryogenic fluid management during feed line quenching operation
	ELSEIF (option_FB == HT_miropolskii) THEN
	 	Remix = abs(m/A)*Dh/viscg*(x+(rhog/rhof)*(1-x))
		Y=max(0,1-0.1*(rhof/rhog-1)**0.4*(1-x)**0.4)
		Prg = cpg*viscg/kg
	 	htcFB = 0.023*Remix**0.8*Prg**0.4*Y*kg/Dh

	-- D.C. Groeneveld, Post-Dryout Heat Transfer at Reactor Operating Conditions
	ELSEIF (option_FB == HT_groeneveld) THEN
	 	Remix = abs(m/A)*Dh/viscg*(x+(rhog/rhof)*(1-x))
		Y=max(1e-5,1-0.1*(rhof/rhog-1)**0.4*(1-x)**0.4)
		Prg = cpg*viscg/kg
	 	htcFB = 0.00109*Remix**0.989*Prg**1.41*Y**(-1.15)*kg/Dh

	-- NASA/TM–2016–219093 
	ELSEIF (option_FB == HT_bromleymod) THEN
	 	dT = abs(Tw-Tsat)
	 	h1=0.62*((kg**2*rhog*Grav*(rhof-rhog)*glv*cpg)/(Dh*dT*Prg))**(1/4)
		htcFB = 2*h1*max(0,(1-1.04*x**2))*(1+0.2*abs(m/A)**0.1)


	END IF  -- closes ELSEIF (option = ...)

	htc1ph = htc_tube_sp(m, A, Dh, kf, viscf, cpf)
	-- this check prevents the heat transfer coefficient in FB regime
	-- to be larger than the one with single phase liquid flow
	IF (htcFB>htc1ph) THEN
		flag = 1
		IF (option_FB != HT_const_FB) THEN -- bypass check if htc is given
			htcFB = htc1ph
		END IF
	ELSE
		flag = 0
	END IF

RETURN htcFB
END FUNCTION
 


-- Function computes the rewet temperature
FUNCTION REAL ht_fun_Twet
(
	IN ENUM HT_OPT_CHILL_TW option_TW	   		"Heat transfer option - chilldown - Rewet temperature",
	IN REAL TW_dat					UNITS u_K				"Data for the heat transfer coefficient if ht_option_TW = ht_const_TW (K)",
	IN REAL m								UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A								UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh							UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN REAL p								UNITS u_Pa			"pressure (Pa)",
	IN REAL T								UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat						UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr							UNITS u_K      	"Critical temperature (K)",
	IN REAL hf							UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg							UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho							UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof						UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog						UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x								UNITS no_units	"quality (-)",
	IN REAL visc						UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf						UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg						UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k								UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf							UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg							UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp							UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf							UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg							UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	IN REAL sigma						UNITS u_N_m			"Surface tension (N/m)",
	-- wall properties
  IN REAL rhos						UNITS u_kg_m3  	"wall material density",
  IN REAL cps							UNITS u_J_kgK 	"wall material specific heat",
  IN REAL ks							UNITS u_W_mK		"wall material thermal conductivity"
)
 
DECLS
  REAL Twet
	REAL We
 	REAL solid, d, erfc_a, Tms

BODY 
  -- dat
	IF (option_TW == HT_const_TW) THEN
		Twet = TW_dat
	
	-- S. Darr et al., Numerical Simulation of the Liquid Nitrogen Chilldown of a Vertical Tube, 2015 DOI:10.2514/6.2015-0468
	ELSEIF (option_TW == HT_darr2015_TW) THEN
		solid = ks*rhos*cps
		-- closed form approximation of the erfc function, from Ren 2007, https://doi.org/10.1002/asl.154
		erfc_a = 2.9110
		d = erfc_a/((erfc_a-1)*sqrt(PI*(1751.5*sqrt(1/solid))**2)+sqrt(PI*(1751.5*sqrt(1/solid))**2+erfc_a**2))
	 	Tms = 0.844*Tcr -- maximum superheat 
	 	Twet = Tsat + 0.29/d*(Tms-Tsat)*(1+0.279*abs(m/A)**(0.49))
	
	-- J.J. Carbajo, A study on the rewetting temperature, 1984
	ELSEIF (option_TW == HT_kalinin_TW) THEN
		solid = sqrt(kf*rhof*cpf/(ks*rhos*cps))
		Twet = Tsat + (Tcr-T)*1.65*(0.1+1.5*sqrt(solid)+0.6*(solid)**2)
	
	-- S. Darr et al., Rewet Temperature Correlations for Liquid Nitrogen Boiling Pipe Flows Across Varying Flow Conditions and Orientations
	ELSEIF (option_TW == HT_darr2019_TW) THEN 
		solid = ks*rhos*cps
		erfc_a = 2.9110
		d = erfc_a/((erfc_a-1)*sqrt(PI*(1751.5*sqrt(1/solid))**2)+sqrt(PI*(1751.5*sqrt(1/solid))**2+erfc_a**2))
		We = (m/A)**2*Dh/(rhof*sigma)
		Tms = 0.844*Tcr
		Twet = (Tsat + 0.29/d*(Tms-Tsat))*(1+0.06*We**0.208)		
	
	END IF
 
 	Twet = max(Twet,Tsat+2)

RETURN Twet
END FUNCTION
  
 
 
-- Function computes the nucleate boiling onset temperature
FUNCTION REAL htc_fun_Ton
(	
	IN ENUM HT_OPT_CHILL_Ton option_Ton			"Heat Transfer option - Nucleate boiling onset model",
	IN REAL Ton_dat   			UNITS u_K    	  "Data for the heat transfer coefficient if ht_option_Ton = ht_const_Ton (K)", 
	IN REAL m								UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A								UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh							UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN REAL p								UNITS u_Pa			"pressure (Pa)",
	IN REAL T								UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat						UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr							UNITS u_K     	"Critical temperature (K)",
	IN REAL hf							UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg							UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho							UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof						UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog						UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x								UNITS no_units	"quality (-)",
	IN REAL visc						UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf						UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg						UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k								UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf							UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg							UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp							UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf							UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg							UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	IN REAL sigma						UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Twet						UNITS u_K    	  "Rewetting temperature (K)"
)

DECLS
	REAL Ton
	REAL B, hcb, Prl, F, glv, c1

BODY
  glv = hg-hf -- latent heat of fusion
	
	-- dat
	IF (option_Ton == HT_const_Ton) THEN
		Ton = Ton_dat
	
	-- S. Darr et al., Numerical Simulation of the Liquid Nitrogen Chilldown of a Vertical Tube, 2015 DOI:10.2514/6.2015-0468
	ELSEIF (option_Ton == HT_darr2015_Ton) THEN
		Ton = Tsat + 0.0071*p/1000 +5
	
	-- T_sat
	ELSEIF (option_Ton == HT_Tsat_Ton) THEN
		Ton = Tsat + 5 -- abritrary constant just to set a value higher than Tsat
	
	-- Frost correlation, from L.-D. Huang, Evaluation of onset of nucleate boiling models, 2009
	ELSEIF (option_Ton == HT_frost_Ton) THEN
		c1 = 1 --(to be precise this contant should be fluid-dependent)
		B = rhog*glv*kf/(8*c1*sigma*Tsat)
		hcb = htc_tube_sp(m, A, Dh, kf, viscf, cpf) -- should be the heat transfer coeff before bubble activation
		-- since the model is developed for boiling and not chilldown, the single phase coeff is employed
		Prl = cpf*viscf/kf
		F = hcb*Prl**2/(2*B)
		Ton = Tsat + F*(1+sqrt(1+2*(Tsat-T)/F))
	
	END IF
	
	Ton = min(Twet-1,Ton)
	Ton = max(Ton,Tsat+1)

RETURN Ton
END FUNCTION



-- Function computes the forced transition boiling heat transfer coefficient
FUNCTION REAL htc_fun_TB
(
	IN ENUM HT_OPT_CHILL_TB option_TB				"Heat transfer option - chilldown - Transition boiling model",
	IN REAL htc_TB_dat	   	UNITS u_W_m2K		"Data for the heat transfer coefficient if ht_option_TB = ht_const_TB (W/m^2*K)",
	IN REAL m								UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A								UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh							UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN REAL p								UNITS u_Pa			"pressure (Pa)",
	IN REAL T								UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat						UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr							UNITS u_K       "Critical temperature (K)",
	IN REAL hf							UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg							UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho							UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof						UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog						UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x								UNITS no_units	"quality (-)",
	IN REAL visc						UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf						UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg						UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k								UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf							UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg							UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp							UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf							UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg							UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	IN REAL sigma						UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Twet						UNITS u_K       "Rewetting temperature (K)",
	IN REAL Tw							UNITS u_K       "Wall temperature (K)"
)

DECLS
	REAL htcTB, theta

BODY
	-- dat
	IF (option_TB == HT_const_TB) THEN 
		htcTB = htc_TB_dat
	
	-- S. Darr et al., Numerical Simulation of the Liquid Nitrogen Chilldown of a Vertical Tube, 2015 DOI:10.2514/6.2015-0468
	ELSEIF (option_TB == HT_darr2015_TB) THEN
		theta = max(0,(Twet-Tw)/(Twet-Tsat))
		theta = min(theta,1)
		htcTB = 0.523*theta**(0.39)*htc_tube_boiling_Chen(m, x, A, Dh, \
         p, T, Tsat, hf, hg, kf, rhof, rhog, viscf, viscg, cpf, sigma, Tw)
			
	END IF

RETURN htcTB

END FUNCTION



-- Function computes the forced nucleate boiling heat transfer coefficient
FUNCTION REAL htc_fun_NB
(
	IN ENUM HT_OPT_CHILL_NB option_NB			"Heat transfer option - chilldown - Transition boiling model",
	IN REAL htc_NB_dat		UNITS u_W_m2K		"Data for the heat transfer coefficient if ht_option_TB = ht_const_TB (W/m^2*K)",
	IN REAL m							UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A							UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh						UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN REAL p							UNITS u_Pa			"pressure (Pa)",
	IN REAL T							UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat					UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr						UNITS u_K       "Critical temperature (K)",
	IN REAL hf						UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg						UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho						UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof					UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog					UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x							UNITS no_units	"quality (-)",
	IN REAL visc					UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf					UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg					UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k							UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf						UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg						UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp						UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf						UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg						UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	IN REAL sigma					UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Twet					UNITS u_K       "Rewetting temperature (K)",
	IN REAL Tw						UNITS u_K       "Wall temperature (K)"
)

DECLS
	REAL htcNB

BODY
	-- dat
	IF (option_NB == HT_const_NB) THEN 
		htcNB = htc_NB_dat
	
	-- Chen
	ELSEIF (option_NB == HT_Chen) THEN
		htcNB = htc_tube_boiling_Chen(m, x, A, Dh, \
            p, T, Tsat, hf, hg, kf, rhof, rhog, viscf, viscg, cpf, sigma, Tw)
			
	END IF

RETURN htcNB
END FUNCTION



FUNCTION REAL htc_fun_Tsens
(
	IN REAL m						UNITS u_kg_s		"Average mass flow through volume (kg/s)",
	IN REAL A						UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh					UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN REAL p						UNITS u_Pa			"pressure (Pa)",
	IN REAL T						UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat				UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr					UNITS u_K       "Critical temperature (K)",
	IN REAL hf					UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg					UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho					UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof				UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog				UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x						UNITS no_units	"quality (-)",
	IN REAL visc				UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf				UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg				UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k						UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf					UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg					UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp					UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf					UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg					UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	IN REAL sigma				UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Twet				UNITS u_K       "Rewetting temperature (K)",
	IN REAL Tw					UNITS u_K       "Wall temperature (K)",
	IN REAL T0					UNITS u_K				"Initial wall temperature (K)"
)

DECLS
	REAL Tsens, c

BODY
	IF (Tw>T) THEN
		c = 1-(Tw-T)/(T0-Tsat)
		Tsens = c*T + (1-c)*Tw
	
	ELSE 
		Tsens = T
	
	END IF
	
	Tsens = min(Tsens, Tw)

RETURN Tsens
END FUNCTION



FUNCTION REAL htc_fun_chilldown
(
	-- options
	IN ENUM HT_OPT_CHILL    	option_HT     	"Heat transfer option",
	IN ENUM HT_OPT_CHILL_FB 	option_FB	    	"Heat transfer option - chilldown - Film boiling model",
	IN ENUM HT_OPT_CHILL_TB 	option_TB	    	"Heat transfer option - chilldown - Transition boiling model",
	IN ENUM HT_OPT_CHILL_NB 	option_NB	   	  "Heat transfer option - chilldown - Nucleate boiling model",
	IN ENUM HT_OPT_CHILL_TW 	option_TW	   	  "Heat transfer option - chilldown - Rewet temperature",
	IN ENUM HT_OPT_CHILL_Ton 	option_Ton		  "Heat Transfer option - Nucleate boiling onset model",
	IN ENUM HT_OPT_CHILL_Qcr  option_qcr    	"Heat transfer option - Critical heat flux", 
	IN REAL htc_dat           UNITS u_W_m2K 	"Data for heat transfer option if ht_opt = ht_dat",
	IN REAL htc_FB_dat				UNITS u_W_m2K	  "Data for the heat transfer coefficient if ht_option_FB = ht_const_FB (W/m^2*K)",
	IN REAL htc_TB_dat				UNITS u_W_m2K	  "Data for the heat transfer coefficient if ht_option_TB = ht_const_TB (W/m^2*K)",
	IN REAL htc_NB_dat				UNITS u_W_m2K	  "Data for the heat transfer coefficient if ht_option_NB = ht_const_NB (W/m^2*K)",
	IN REAL TW_dat						UNITS u_K		  	"Data for the heat transfer coefficient if ht_option_TW = ht_const_TW (K)",
	IN REAL Ton_dat			   		UNITS u_K       "Data for the heat transfer coefficient if ht_option_Ton = ht_const_Ton (K)", 
	IN REAL qcr_dat           UNITS u_W_m2    "Imposed critical heat flux if ht_option_qcr == ht_const",
	IN REAL h_f								UNITS no_units  "Multiplier of the heat transfer coefficient",
	--
	IN REAL m						   		UNITS u_kg_s	  "Average mass flow through volume (kg/s)",
	IN REAL m_dot             UNITS u_kg_s2   "Derivative of the mass flow rate",
	IN REAL A									UNITS u_m2			"Cross area (m^2)",
	IN REAL Dh								UNITS u_m				"Hydraulic diameter (m)",
	--fluid properties
	IN ENUM Phase phase				UNITS no_units	"Fluid phase (-)",
	IN REAL p									UNITS u_Pa			"pressure (Pa)",
	IN REAL T									UNITS u_K				"fluid temperature (K)",
	IN REAL Tsat							UNITS u_K				"saturation temperature (K)",
	IN REAL Tcr								UNITS u_K    	  "Critical temperature (K)",
	IN REAL h									UNITS u_J_kg		"Enthalpy of mixture (J/",
	IN REAL hf								UNITS u_J_kg		"Enthalpy of saturated liquid (J/kg)",
	IN REAL hg								UNITS u_J_kg		"Enthalpy of saturated vapor  (J/kg)",
	IN REAL rho								UNITS u_kg_m3		"fluid density (kg/m^3)",
	IN REAL rhof							UNITS u_kg_m3		"density of liquid (kg/m^3)",
	IN REAL rhog							UNITS u_kg_m3		"density of vapor (kg/m^3)",
	IN REAL x									UNITS no_units	"quality (-)",
	IN REAL visc							UNITS u_Pas			"viscosity  (Pa*s)",
	IN REAL viscf							UNITS u_Pas			"viscosity of liquid (Pa*s)",
	IN REAL viscg							UNITS u_Pas			"viscosity of vapor (Pa*s)",
	IN REAL k									UNITS u_W_mK		"thermal conductivity  (W/m*K)",
	IN REAL kf								UNITS u_W_mK		"thermal conductivity of liquid (W/m*K)",
	IN REAL kg								UNITS u_W_mK		"thermal conductivity of vapor  (W/m*K)",
	IN REAL cp								UNITS u_J_kgK		"specific heat (J/kg*K)",
	IN REAL cpf								UNITS u_J_kgK		"specific heat of liquid (J/kg*K)",
	IN REAL cpg								UNITS u_J_kgK		"specific heat of vapor (J/kg*K)",
	--wall properties
	IN REAL sigma							UNITS u_N_m			"Surface tension (N/m)",
	IN REAL Tw								UNITS u_K				"wall temperature (K)",
	IN REAL Grav							UNITS u_m_s2		"Gravitational acceleration (m/s^2)",
	IN REAL xwall							UNITS u_m   		"distance from inlet",
	IN REAL rhos							UNITS u_kg_m3 	"wall material density",
	IN REAL cps								UNITS u_J_kgK  	"wall material specific heat",
	IN REAL ks								UNITS u_W_mK		"wall material thermal conductivity",
	IN REAL T0								UNITS u_K				"Initial wall temperature",
	OUT REAL check  			 	  UNITS no_units 	"Boiling regime identification flag", 
	OUT REAL Twet             UNITS u_K      	"Rewetting temperature",
	OUT REAL Ton              UNITS u_K      	"Nucleate boiling onset temperature",
	OUT REAL q_cr							UNITS u_W_m2    "Critical heat flux",
	OUT REAL Tsens						UNITS u_K				"Sensor temperature"
	--OUT REAL flag
)

DECLS
	REAL htc									UNITS u_W_m2K		"Heat transfer coefficient (W/m^2*K)"
	REAL T_cr									UNITS u_K				"Wall temperature for critial heat flux (K)"
	REAL Ph						 			  UNITS no_units	"Phase number (-)"
	REAL We = 0								UNITS no_units 	"Weber number"
	REAL q1 = 0								UNITS u_W_m2   	"Nucleate boiling heat flux"
	REAL h1 = 0								UNITS u_W_m2K  	"Nucleate boiling heat transfer coefficient"
	REAL glv = 0							UNITS u_J_kg   	"Latent heat of vaporization"
	REAL htc_pool, htcFB, htcNB									UNITS u_W_m2K		"Heat transfer coefficients"
	REAL htcTB, htc1ph, htc_gas  								UNITS u_W_m2K  	"Heat transfer coefficients"
	REAL alp0, alp1, alp2, alp21, alp22, alp3  	UNITS no_units 
	REAL beta0, beta1, beta2, beta3, gamma0			UNITS no_units
	REAL x_gas, xeq														  UNITS no_units
	REAL T_CHF, G_pool													UNITS no_units
	REAL IPOTESI																UNITS no_units
	ENUM COND_Option COND_Broyko
	REAL flag
BODY
	glv = hg-hf -- latent heat of fusion
	xeq = (h-hf)/glv -- equilibrium quality
	
	-- Sensor temperature modeling	 
	Tsens = htc_fun_Tsens(m, A, Dh,\
				  p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
			    Twet, Tw, T0)
	 
	-- Constant heat transfer coefficient
	IF (option_HT == HT_dat) THEN 
		htc = htc_dat
		RETURN htc
	
	ELSEIF (option_HT == HT_TUBE) THEN
		htc = htc_tube(COND_Broyko, m, A, Dh, phase, p, T, Tsat, hf, hg, x, rhof, rhog,\
                           visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma, Tw)
		htc = htc*h_f
		RETURN htc
	END IF	 

	-- FORCED CONVECTION BOILING
	-- Single phase (gas)
	htc_gas = htc_tube_sp(m, A, Dh, kg, viscg, cpg) 

	-- Rewetting temperature
	Twet = ht_fun_Twet(option_TW, TW_dat, m, A, Dh,\
										 p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
										 rhos, cps, ks)

	-- Film boiling
	htcFB = htc_fun_FB(option_FB, htc_FB_dat, m, A, Dh,\
										 p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
										 Tw, Grav, xwall, Twet, T0, flag)
		  
	-- Onset of nucleate boiling	
 	Ton = htc_fun_Ton(option_Ton, Ton_dat, m, A, Dh,\
										p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
										Twet)

	-- Transition boiling			
 	htcTB = htc_fun_TB(option_TB, htc_TB_dat, m, A, Dh,\
    									p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
											Twet, Tw)

	-- Nucleate boiling	
 	htcNB = htc_fun_NB(option_NB, htc_NB_dat, m, A, Dh,\
										 p, T, Tsat, Tcr, hf, hg, rho, rhof, rhog, x, visc, viscf, viscg, k, kf, kg, cp, cpf, cpg, sigma,\
										 Twet, Tw)
		
	-- Single phase (liquid)
	htc1ph = htc_tube_sp(m, A, Dh, kf, viscf, cpf)

	beta0 = 50
	beta1 = 0.1
	beta2 = 2.5e-5
	x_gas = 0.9
		
	-- Critical heat flux
	We = (max(m,1e-3)/A)**2*xwall/(rhof*sigma)
	q_cr = 0.0527*(m/A)*glv*We**(-0.2894)
	h1 = htc_tube_boiling_Chen(m, x, A, Dh, \
	                          p, T, Tsat, hf, hg, kf, rhof, rhog, viscf, viscg, cpf, sigma, Tw)
	q1 = h1*(Tw-T)
	
	-- Weights
	alp0  = 0.5*(1 + tanh(beta0*(xeq-x_gas)))
	alp1  = 0.5*(1 - tanh(beta1*(Twet - Tw)))
	alp2  = 0.5*( tanh(beta1*(Twet - Tw)) - tanh(beta1*(Ton - Tw)) )
	alp21 = 0.5*(1 - tanh(beta2*(q1 - q_cr)))
	alp22 = 0.5*(1 - tanh(beta2*(q_cr - q1)))
	alp3  = 0.5*(1 - tanh(beta1*(Tw - Ton)))

	htc = alp0*htc_gas + (1-alp0)*(alp1*htcFB + alp2*( alp21*htcNB + alp22*htcTB ) + alp3*htc1ph)
	htc = abs(htc)*h_f		
  check = alp0*0 +	(1-alp0)*(alp1*1 + alp2*( alp21*21 + alp22*22 ) + alp3*3)
	

				
RETURN htc
END FUNCTION